package com.orangepixel.tinypuncher.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.orangepixel.tinypuncher.myCanvas;

/**
 * PC startup
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */
public class DesktopLauncher {
	
	static myCanvas startCanvas;
	
	public static void main (String[] args) {
	
		
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Tiny Puncher - Orangepixel prototype code";
		cfg.resizable=true;
		cfg.vSyncEnabled=true;

		cfg.width = 1280;
		cfg.height = 640;
		cfg.fullscreen=false;

		
		startCanvas=new myCanvas();
		
		new LwjglApplication(startCanvas, cfg);
		
		for (String s: args) {
			if (s.toLowerCase().contains("nocontroller")) {
				startCanvas.argument_noController=true;
				cfg.title+=" - nocontroller";
			}
			
			if (s.toLowerCase().contains("forcewindow")) {
				startCanvas.argument_forceWindowed=true;
				cfg.title+=" - windowed";
			}			
        }
	}
}
