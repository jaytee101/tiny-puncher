package com.orangepixel.social;

import com.orangepixel.tinypuncher.PlayerProfile;

public interface Social {
	
	
	public boolean isConnected();
	
	
	public void initSocial();
	public void loginSocial();
	public boolean isLoggedIn();
	
	public void disposeSocial();
	
	public void uploadScore(String leaderBoard, int score, boolean keepBest);
	public void uploadAchievement(int achievementID);
	public void uploadPartialAchievement(int achievementID, int currentStep, int maxStep);
	
	public String getUserName();
	public void getScores(String leaderBoard);
	
	public void uploadDaily(String leaderBoard, int score);
	
	// called on every frame update, steam requires this to process the callbacks
	public void processInfo(); 
	
	public int getMaxHighscoreCount();
	public int getHighScores(int idx);
	public String getHighScoreName(int idx);
	public boolean doneDownloadingScores();
	public boolean doneUploadingScore();
	public boolean noLeaderboardFound();
	
	// cloud saving
	public void cloudSave(PlayerProfile myProfile);
	public void cloudLoad(PlayerProfile myProfile);
	public void uploadFile(String fileName, byte[] myBytes);
	public void downloadFile(String fileName);
	public void deleteFile(String fileName);
	
	// steamworks specific
	public void publishFile(String pubID, String fileName, String screenshotName, String title, String description);
	public String[] fetchFileList(String extension);
	public void fetchFiles();
	public boolean completedPublish();
	public String getPublishFileID();
	
}
