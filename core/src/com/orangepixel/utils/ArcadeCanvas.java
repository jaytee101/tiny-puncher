package com.orangepixel.utils;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Graphics.DisplayMode;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.orangepixel.controller.GameInput;
import com.orangepixel.social.Social;
import com.orangepixel.tinypuncher.Globals;

/**
 * Base Orangepixel framework class, handles the init and core loop
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */


public class ArcadeCanvas implements ApplicationListener {

	public static final boolean IS_MAC = System.getProperty("os.name").toLowerCase().contains("mac");
	public static final boolean IS_WINDOWS = System.getProperty("os.name").toLowerCase().contains("windows");
	public static final boolean IS_LINUX = System.getProperty("os.name").toLowerCase().contains("linux");
	
	public boolean argument_noController=false;
	public boolean argument_forceWindowed=false;
	
	public boolean isDesktop=false;
	public boolean isAndroid=false;
	public boolean isIOS = false;
	public static boolean isAndroidTV = false;

	public boolean isFullScreen=false;
	
	public final static int	ININIT = 1, 			
							INSPLASH = 2,											
							INMENU = 3,
							INPAUSE = 4,
							INGAME = 20,
							INLOADER = 21;
		

	
	// system value, if true it uses the Light class for rendering lights
	public static boolean isLightRendering;	 // switch this on/off when needed
	
	
	// resolution settings (desktop) 
	public final static int		propWinID = 0,
								propWinWidth = 1,
								propWinHeight = 2;

	// order the list based on resolution, but keep propWinID unchanged 
	// this way we can add new resolutions without screwing up existing user settings
	public final static int[][] windowedModes=new int[][] 
	{
			{0,1024, 640}, 
			{1,1024, 640}, 
			{2,1080, 640}, 
			{3,1152, 640}, 
			{4,1280, 640}, 
			{5,1280, 768}, 
			{6,1440, 896},
			{7,1600, 896},
			{8,1680, 1024},
			{9,1920, 1152},
			{999,9999, 9999}
	};
	
	public static int windowedModeID=1;	
	

	
	// Collection of sprites (woohoo!)
	public static Texture[] sprites;
	
	
	// Social - platform specific (if null, nothing is attached/available)
	public Social mySocial=null;

	
	// global variables
	public int GameState;
	public int worldTicks;
	public boolean secondPassed;
	public boolean worldTickUpdate;
	// used to signal a paused state
	public boolean paused;
	
	
	// I keep alt+enter in for toggling fullscreen at all times
	public boolean						switchFullScreen;
	public boolean						switchFullScreenLocked;
	
	public boolean						triggerFullScreenSwitch=false;


	
	// system variables (framerate, mouse handling ,etc)
	public int myFramerate;
	private int fpsCount;
	private long loopEnd;
	private long loopPause;
	private long loopTickUpdate;
	private static int displayW;
	private static int displayH;
		
	
	// rendering buffers
	FrameBuffer m_fbo;
	TextureRegion m_fboRegion=null;
	
	
	
	
	
	@Override
	public void create() {
		displayW=(int)Gdx.graphics.getWidth();
		displayH=(int)Gdx.graphics.getHeight();

		isDesktop=false;
		isAndroid=false;
		isIOS=false;

		switch (Gdx.app.getType()) {
			case Android:
				isAndroid=true;
				GameInput.IS_ANDROID=true;
				GameInput.controller1.isTouchscreen();
			break;
			
			case iOS:
				isIOS=true;
				GameInput.IS_IOS=true;
				GameInput.controller1.isTouchscreen();
			break;
				
			
			default:
				GameInput.isDesktop=true;
				isDesktop=true;
				GameInput.controller1.isKeyboard();
			break;
		}

		

		// init our render class, set it to a pixel-art size
		// find height closest to 160, and matching width
		Render.initRender();
		Render.height=(int)(displayH/Math.floor(displayH/160));
		Render.width=(int)(displayW/(displayH/ (displayH/Math.floor(displayH/160)) ));
		
		// we use less, but make sure we have enough slots
		sprites=new Texture[32];
		

		Gdx.app.getInput().setInputProcessor(GameInput.myProcessor);
		
		Gdx.input.setCatchBackKey(true);
		Gdx.input.setCatchMenuKey(true);

		if (!argument_noController)  GameInput.initControllers();
		
		loopPause = System.currentTimeMillis();
		loopTickUpdate=System.currentTimeMillis();

		GameState=ININIT;		
	}
	
	

	
	

	@Override
	public void dispose() {
		if (mySocial!=null) mySocial.disposeSocial();
		Render.batch.dispose();
	}

	
	

	public static int PowerOf2(int n) {
		  int k=1;
		  while (k<n) k*=2;
		  return k;
	}	

	
	
	
	@Override
	public void resize(int width, int height) {
		displayW=width;
		displayH=height;

		Render.fullScreenWidth=width;
		Render.fullScreenHeight=height;

		Render.height=(int)(displayH/Math.floor(displayH/160));
		Render.width=(int)(displayW/(displayH/ (displayH/Math.floor(displayH/160)) ));

		Globals.debug("Display:"+displayW+"x"+displayH+"  -  pixels:"+Render.width+"x"+Render.height);
		
		generateFrameBuffers();

		// trigger a reinit of the controllers
	    if (GameInput.controllersFound>0) Controllers.clearListeners();
	    GameInput.controllersFound=0;
	}
	
	
	
	
	public final void generateFrameBuffers() {
		if (m_fbo!=null) m_fbo.dispose();
	    m_fbo = new FrameBuffer(Format.RGB888, PowerOf2(Render.width), PowerOf2(Render.height), false);
	    m_fbo.getColorBufferTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	    m_fboRegion = new TextureRegion(m_fbo.getColorBufferTexture(),0,m_fbo.getHeight()-Render.height,Render.width,Render.height);
	    m_fboRegion.flip(false, false);

	    
	    // Light (alpha blending) buffer
	    Light.createBuffers(Render.width, Render.height);
	    
	    Render.camera.setToOrtho(true, Render.width,Render.height);
	    Render.camera.update();
	}

	
	

	
	
	
	
	
	
	// openGL Render() - 60 frames per second
	// this also calls our logic and gameloop
	@Override
	public void render() {
		
		if (Gdx.input.isKeyPressed(Keys.ENTER) 
				&& (Gdx.input.isKeyPressed(Keys.ALT_LEFT) || Gdx.input.isKeyPressed(Keys.ALT_RIGHT)) 
		)  {
			if (!switchFullScreenLocked) {
				switchFullScreen=true;
				switchFullScreenLocked=true;
				triggerFullScreenSwitch=true;
			}
		} else {
			switchFullScreen=false;
			switchFullScreenLocked=false;
		}		
		
		
		if (triggerFullScreenSwitch) {
			if (!GameInput.controller1.isMouse || (!GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked)) {
				setDisplayMode(1080, 640, !isFullScreen);
				triggerFullScreenSwitch=false;
			}
		}
		
		
		if (!GameInput.controller1.isMouse && GameInput.hideMouse==0) {
			if (isFullScreen) Gdx.input.setCursorCatched(true);
		}
		
		// global worldticking / heart beat of game
    	worldTicks++;																
		if (worldTicks>1000) worldTicks=0;											
		
		// transform touch location to pixel-art screen locations
		GameInput.touchX = -1;
		GameInput.touchY = -1;
		if (GameInput.mTouchX[0] >= 0 && GameInput.mTouchY[0] >= 0) {
			GameInput.touchX=GameInput.mTouchX[0];
			GameInput.touchY=GameInput.mTouchY[0];
		}

		

		// render to pixel-art framebuffer
        m_fbo.begin();		
        	Gdx.gl.glDisable(GL20.GL_BLEND);
        	Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
        	Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        	Render.camera.setToOrtho(true, m_fbo.getWidth(), m_fbo.getHeight());
        	Render.camera.update();
	        
        	Render.batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
        	Render.setAlpha(255);
        	
        	// gamelogic + render
    		if (GameState==INGAME) GameLoop();
    		else LogicLoop();
			
			if (Render.globalTexture!=null) {
				Render.batch.end();
				Render.globalTexture=null;
			}
		m_fbo.end();

			
		if (isLightRendering) {
        		
		    Light.render();
	        
			// render light buffer on top of pixelart-buffer
			m_fbo.begin();
				Render.batch.begin();
			    Render.batch.setBlendFunction(GL20.GL_DST_COLOR, GL20.GL_ZERO);
			    Render.batch.setColor(1,1,1,1);
				Render.batch.draw(Light.lightBufferRegion, 0, 0, Render.width, Render.height);     
				Render.batch.end();
			m_fbo.end();		        
        }		
			

		// render the post-light stuff (statusbars, etc)
		m_fbo.begin();
	        Render.batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		
			renderPostLights();
			if (Render.globalTexture!=null) {
				Render.batch.end();
				Render.globalTexture=null;
			}
		m_fbo.end();
		
		 

		Render.camera.setToOrtho(true, displayW, displayH);
		Render.camera.update();
		
		 // render pixel-art buffer to screen
		Render.batch.setProjectionMatrix(Render.camera.combined);
		Render.batch.begin();        
		Render.batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		Render.batch.setColor(1,1,1,1);
		Render.batch.draw(m_fboRegion, 0, 0, displayW, displayH);               
		Render.batch.end();

		 
		 
		fpsCount++;
		if (loopEnd-loopPause>1000) {
			secondPassed=true;
			loopPause=loopEnd;
			if (GameInput.controllersFound==0) GameInput.initControllers();
			myFramerate=fpsCount;
			fpsCount=0;
		}
		
		
		if (loopEnd-loopTickUpdate>300) {
			worldTickUpdate=true;
			loopTickUpdate=loopEnd;
		}
		

		loopEnd = System.currentTimeMillis();
		

		if (!GameInput.controller1.isMouse && GameInput.hideMouse==999) GameInput.hideMouse=128;
		if (GameInput.hideMouse>0) {
			if (GameInput.hideMouse!=999) GameInput.hideMouse--;
		} else {
			// hide mouse
			GameInput.cursorX=-1;
			GameInput.cursorY=-1;
		}


		if (mySocial!=null) mySocial.processInfo();
	}
	

	
	
	public DisplayMode[] modes; 
	public int currentModeID=-1;
	private int fallbackModeID;
	
	
	public void setDisplayMode(int width, int height, boolean fullscreen) {
		
		if (argument_forceWindowed) fullscreen=false;
		
		// return if requested DisplayMode is already set
        if ((Gdx.app.getGraphics().getWidth() == width) && 
			(Gdx.app.getGraphics().getHeight() == height) && 
			(Gdx.app.getGraphics().isFullscreen() == fullscreen)) {
			return;
		}
		
        if (fullscreen) {
        	// just use current desktop display width*height so we know it's fullscreen correctly
        	width=Gdx.app.getGraphics().getDesktopDisplayMode().width;
        	height=Gdx.app.getGraphics().getDesktopDisplayMode().height;
        }

        
		DisplayMode targetDisplayMode = null;
		DisplayMode fallBackMode = null;

		float 	baseRatio=(float)width/(float)height;	// 1,5
		float	currentRatio;
		
		
		modes = Gdx.app.getGraphics().getDisplayModes();
		
		if (fullscreen) {
			int freq = 0;
			
			for (int i=0;i<modes.length;i++) {
				DisplayMode current = modes[i];
				currentRatio=(float)current.width/(float)current.height;
				
				if (current.width>=width && current.height>=height && currentRatio>=baseRatio) {
					if (current.refreshRate >= freq && current.bitsPerPixel >= Gdx.app.getGraphics().getDesktopDisplayMode().bitsPerPixel) {
						targetDisplayMode = current;
						currentModeID=i;
						freq = targetDisplayMode.refreshRate;
					}
				} else {
					// find a "best" resolution if we can't find an exact math on ratio/w/h/bbp
					if ((current.bitsPerPixel == Gdx.app.getGraphics().getDesktopDisplayMode().bitsPerPixel ) &&
					    (current.refreshRate == Gdx.app.getGraphics().getDesktopDisplayMode().refreshRate )) {
						fallBackMode = current;
						fallbackModeID=i;
					}
				}
			}
			
			if (targetDisplayMode!=null) {
				Gdx.app.getGraphics().setDisplayMode(targetDisplayMode.width, targetDisplayMode.height,true);
			} else if (fallBackMode!=null) {
				Gdx.app.getGraphics().setDisplayMode(fallBackMode.width, fallBackMode.height,true);
				currentModeID=fallbackModeID;
			}
		} else {
			Gdx.app.getGraphics().setDisplayMode(width,height, false);
		}

		
		isFullScreen=fullscreen;
		
		if (isFullScreen) {
			Gdx.input.setCursorPosition((displayW>>1),(displayH>>1));
			Gdx.input.setCursorCatched(true);
		} else {
			Gdx.input.setCursorPosition((displayW>>1),(displayH>>1));
			Gdx.input.setCursorCatched(false);
		}
		
	
		// trigger a reinit of the controllers
		GameInput.controllersFound=0;
	}	
	
	

	/* ==================
	 * fetch the specified ID from the supported window resolution list
	 * ==================	
	 */
	public final int fetchWindowMode(int myID) {
		int i=0;
		while (i<windowedModes.length) {
			if (windowedModes[i][propWinID]==myID) return i;
			i++;
		}
		
		return windowedModes.length;
		
	}
	
	
	
	
	
	
	
	public void pause() {}
	public void resume() {}	
	public void LogicLoop(){};
	public void GameLoop(){};
	public void renderPostLights(){};

	
}
;