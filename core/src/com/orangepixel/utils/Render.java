package com.orangepixel.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack;


/**
 * All video/render/graphic handling goes here
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class Render {

	
	
	
	// resolution 
	public static int width;
	public static int height;
	
	// actual screen resolution (to translate touch or mouse coordinates to game resolution)
	public static int fullScreenWidth;
	public static int fullScreenHeight;
	
	// use a global so we can do setAlpha() and all render calls will use it
	public static int globalAlpha;
	public static int globalRed;
	public static int globalGreen;
	public static int globalBlue;	

	
	// batch rendering, shaperendering (rectangles, fillrectangles)
	public static OrthographicCamera camera;
	
	public static SpriteBatch batch;
	public static ShapeRenderer shapeRenderer;
	
	public static Rect dest=new Rect();
	public static Rect src=new Rect();

	
	// normally you don't need to access this.. bit sloppy but sometimes you do :/
	public static Texture globalTexture=null;
	
	
	
	public final static void initRender() {
		camera = new OrthographicCamera();
		
		batch = new SpriteBatch();
		batch.setBlendFunction(GL20.GL_ONE, -1); //(-1, -1);
		
		shapeRenderer = new ShapeRenderer();
		shapeRenderer.setProjectionMatrix(Render.camera.combined);
		
	}
	
	
	public final static void drawBitmap(Texture sprite) {
		drawBitmap(sprite,Render.src,Render.dest, false);
	}

	public final static void drawBitmap(Texture sprite, boolean flip) {
		drawBitmap(sprite,Render.dest,Render.src, flip);
	}
	
	
	public final static void drawBitmap(Texture sprite, Rect src, Rect dest) {
		drawBitmap(sprite,dest,src, false);
	}
	
	public final static void drawBitmap(Texture sprite, Rect src, Rect dest, boolean flip) {
		if (sprite!=globalTexture) {
			if (globalTexture!=null) batch.end();
			
			batch.begin();
			batch.setProjectionMatrix(camera.combined);
			batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
			
			globalTexture=sprite;
		}

		if (flip)
		{
			dest.left+=dest.width;
			dest.width=-dest.width;
		}

		batch.setColor(1f,1f,1f,globalAlpha/255.0f);
		batch.draw(sprite, dest.left,dest.top,
				dest.width,dest.height,
				src.left,src.top, 
				src.width,src.height,
				
				false,true);
	}	
	
	// use this for sprites needing rotation
	public final static void drawBitmapRotated(Texture sprite,Rect src, Rect dest, float myRotate) {
		if (sprite!=globalTexture) {
			if (globalTexture!=null) batch.end();
			
			batch.begin();
			batch.setProjectionMatrix(camera.combined);
			batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
			
			globalTexture=sprite;
		}


		batch.setColor(1f,1f,1f,globalAlpha/255.0f);

		batch.draw(sprite,dest.left,dest.top,  
					(dest.width>>1), (dest.height>>1),
					dest.width,dest.height,
					1f,1f,
					myRotate, 
					src.left,src.top,
					src.width,src.height,
					false,true);
	}	
	
	
	
	public final static void clipRect(int x, int y, int w, int h) {
		Rectangle scissors = new Rectangle();
		Rectangle clipBounds = new Rectangle(x,y,w,h);
		ScissorStack.calculateScissors(camera, batch.getTransformMatrix(), clipBounds, scissors);
		ScissorStack.pushScissors(scissors);
	}

	
	
	public final static void endClip() {
		ScissorStack.popScissors();
	}	
	
	public final static void setARGB(int alpha, int red, int green, int blue) {
		globalAlpha=alpha;
		globalRed=red;
		globalGreen=green;
		globalBlue=blue;
		Gdx.gl.glClearColor(red/255.0f, green/255.0f, blue/255.0f, alpha/255.0f);
		shapeRenderer.setColor(red/255.0f, green/255.0f, blue/255.0f, alpha/255.0f);
	}
	
	

	public final static void setAlpha(int alpha) {
		globalAlpha=alpha;
		Gdx.gl.glClearColor(1.0f, 1.0f, 1.0f, alpha/255.0f);
		shapeRenderer.setColor(globalRed/255.0f, globalGreen/255.0f, globalBlue/255.0f, alpha/255.0f);
	}
	
	
	
	public final static void drawPaint(int a, int r, int g, int b) {
		if (globalTexture!=null) {
			batch.end();
			globalTexture=null;
		}
		
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
		
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(r/255.0f, g/255.0f, b/255.0f, a/255.0f);
		shapeRenderer.rect(0,0, width, height);
		shapeRenderer.end();
	}

	
	
	public final static void drawRect(int x, int y, int w, int h) {
		if (globalTexture!=null) {
			batch.end();
			globalTexture=null;
		}
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFuncSeparate(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_ONE, GL20.GL_ONE);
		
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Line);
		shapeRenderer.setColor(globalRed/255.0f, globalGreen/255.0f, globalBlue/255.0f, globalAlpha/255.0f);
		shapeRenderer.rect(x,y, w,h);
		shapeRenderer.end();
	}	
	
	
	
	public final static void fillRect(int x, int y, int w, int h) {
		if (globalTexture!=null) {
			batch.end();
			globalTexture=null;
		}
		Gdx.gl.glEnable(GL20.GL_BLEND);
		Gdx.gl.glBlendFuncSeparate(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_ONE, GL20.GL_ONE);
		
		shapeRenderer.setProjectionMatrix(camera.combined);
		shapeRenderer.begin(ShapeType.Filled);
		shapeRenderer.setColor(globalRed/255.0f, globalGreen/255.0f, globalBlue/255.0f, globalAlpha/255.0f);
		shapeRenderer.rect(x,y, w,h);
		shapeRenderer.end();
	}
		
}
