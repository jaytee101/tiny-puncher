package com.orangepixel.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

/**
 * Light class, used for fast 2D light effects (alpha blending textures)
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class Light {
	
	public final static int LightType_Up = 0,
							LightType_Right = 1,
							LightType_Down = 2,
							LightType_Left = 3,
							LightType_SphereTense = 4,
							LightType_Sphere = 5,
							LightType_FLARE = 6,
							
							LightType_CLIP = 7;
	

	public static Light[] myLights = new Light[256];
	public static Color ambientColor=new Color();
	
	// type of light
	int	  lightType;
	
	// position and dimensions
	float x;
	float y;
	float w;
	float h;
	
	float distance;
	
	// colors
	float r;
	float g;
	float b;
	float a;
	
	boolean active;
	
	public static Texture mySprite;
	
	// buffers for rendering the light
	static FrameBuffer lightBuffer;
	static TextureRegion lightBufferRegion;

	
	public final static void initLights(String fileName) {
		for (int i=myLights.length - 1; i >= 0; i--) 
		{
			myLights[i]=new Light();
			myLights[i].setActive(false);
		}
		
		mySprite=new Texture(Gdx.files.internal(fileName), true);
	}
	
	
	
	private static int PowerOf2(int n) {
		  int k=1;
		  while (k<n) k*=2;
		  return k;
	}	

	
	public final static void createBuffers(int width, int height) {
	    if (lightBuffer!=null) lightBuffer.dispose();
	    lightBuffer = new FrameBuffer(Format.RGBA8888, PowerOf2(width), PowerOf2(height), false);
	    lightBuffer.getColorBufferTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
	    lightBufferRegion = new TextureRegion(lightBuffer.getColorBufferTexture(),0,lightBuffer.getHeight()-Render.height,Render.width,Render.height);
	    lightBufferRegion.flip(false, false);
	}

	
	
	// light handling
	public final static void resetLights() {
		for (int i=0; i < myLights.length; i++) {
			myLights[i].setActive(false);
		}
	}
	
	// FakeLight system
	public final static void setAmbientLight(float red, float green, float blue, float alpha) {
		ambientColor.set(red,green,blue,alpha/4); 
	}
	

	public final static void addLight(int ax, int ay, float myDist, int myType, float myR, float myG, float myB, float myA) {
		int i=0;
		
		// skip if out of screen area
//		if (myx+myDist<0 || myx-myDist>width || myy+myDist<0 || myy+myDist>height) return;

		// FakeLight
		while (i < myLights.length && myLights[i].isActive()) i++;
		if (i < myLights.length) {
			myLights[i].setLightType(myType);
			
			myLights[i].setPosition(ax,ay);
			myLights[i].setColor(myR, myG, myB, myA);
			myLights[i].setDistance(myDist);
			myLights[i].setActive(true);
		}

	}			
	
	public Light() {
		active=false;
	}
	
	public int getLightType() {
		return lightType;
	}
	
	public void setLightType(int myType) {
		lightType=myType;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public void setPosition(float ax, float ay) {
		x=ax;
		y=ay;
	}
	
	public Color getColor() {
		return new Color(r,g,b,a);
	}
	
	public void setColor(float ar, float ag, float ab, float aa) {
		r=ar;
		g=ag;
		b=ab;
		a=aa;
	}
	
	public float getDistance() {
		return distance;
	}
	
	public void setDistance(float adistance) {
		distance=adistance;
	}

	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean mActive) {
		active=mActive;
	}
	
	
	
	public final static void render() {
    	lightBuffer.begin();
        Gdx.gl.glClearColor(Light.ambientColor.r,Light.ambientColor.g,Light.ambientColor.b,Light.ambientColor.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
        Render.batch.begin();
		Render.batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE); //GL20.GL_SRC_ALPHA, -1);

		for (int i=0; i<Light.myLights.length; i++) {
			if (Light.myLights[i].isActive()) {
				Render.batch.setColor(Light.myLights[i].getColor());
				
				float tx=Light.myLights[i].getX();
				float ty=Light.myLights[i].getY();
				float tw=(128/100f)*Light.myLights[i].getDistance();
				
				// 64,92
				
				switch (Light.myLights[i].getLightType()) {
					case Light.LightType_Up:
						tx-=((128/100f)*Light.myLights[i].getDistance())/2;
						ty-=((92/100f)*Light.myLights[i].getDistance());
						Render.batch.draw(mySprite, tx,ty,
								tw,tw,
								0,128, 
								128,128,
								false,true);
					break;

					case Light.LightType_Right:
						tx-=((128/100f)*Light.myLights[i].getDistance())/2;
						ty-=((128/100f)*Light.myLights[i].getDistance());
						Render.batch.draw(mySprite,tx,ty,  
								tw/2,tw,
								tw,tw,
								1f,1f,
								90, 
								0,128,
								128,128,
								false,true);
					break;
					

					case Light.LightType_Down:
						tx-=((128/100f)*Light.myLights[i].getDistance())/2;
						ty+=((128/100f)*Light.myLights[i].getDistance());
						Render.batch.draw(mySprite,tx,ty,  
								tw/2,0,
								tw,tw,
								1f,1f,
								180, 
								0,128,
								128,128,
								false,true);
					break;			
					

					case Light.LightType_Left:
						tx-=((128/100f)*Light.myLights[i].getDistance())/2;
						ty-=((128/100f)*Light.myLights[i].getDistance());
						Render.batch.draw(mySprite,tx,ty,  
								tw/2,tw,
								tw,tw,
								1f,1f,
								270, 
								0,128,
								128,128,
								false,true);
					break;							

					
					case Light.LightType_SphereTense:
						tx-=(tw/2);
						ty-=(tw/2);
						Render.batch.draw(mySprite, tx,ty,
								tw,tw,
								256,0, 
								128,128,
								false,true);
					break;
					
					case Light.LightType_FLARE:
						tx-=(tw/2);
						ty-=(tw/2);
						Render.batch.draw(mySprite, tx,ty,
								tw,tw,
								128,128, 
								128,128,
								false,true);
					break;
					
					default:
						tx-=(tw/2);
						ty-=(tw/2);
						Render.batch.draw(mySprite, tx,ty,
								tw,tw,
								0,0, 
								128,128,
								false,true);
					break;
				}
				
			}
		}

			
		Render.batch.end();
	    lightBuffer.end();
	}

}
