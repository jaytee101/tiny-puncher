package com.orangepixel.tinypuncher;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.orangepixel.controller.GameInput;
import com.orangepixel.tinypuncher.ai.Brawlers;
import com.orangepixel.tinypuncher.ai.Monster;
import com.orangepixel.tinypuncher.ai.Scenery;
import com.orangepixel.utils.ArcadeCanvas;
import com.orangepixel.utils.Light;
import com.orangepixel.utils.Render;

/**
 * The main code loops
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class myCanvas extends ArcadeCanvas {

	
	boolean		isCheatOn=false;	 // just so it won't save high-scores during cheat

	public final static String PROFILEID="tinypuncher";

	PlayerProfile activePlayer;

	
	// used for our splash logo/intro
	static Texture	splashImage;
	int 			splashFrame;
	boolean 		splashDone;
	int 			splashAlpha;
	int 			splashYSpeed;
	int 			splashY;	
	
	
	
	Player			myPlayer;
	World			myWorld;
	
	

	
	// global used for drawing, calculating, etc
	static int tx;
	static int ty;
	static int tx2;
	static int ty2;
	static int tile;
	int randx;
	int randy;
	int randz;
	boolean done;
	static boolean foundFirst;
	float percent;
	
	
	// Main game initialisation
	public void init() {
		// create our splash image (Orangepixel logo)
		splashImage=new Texture(Gdx.files.internal("spl2.png"), true);
		splashFrame=0;
		splashDone=false;
		splashAlpha=0;
		splashY=0;
		splashYSpeed=-8;

		// initialise player profile, for saving/loading settings
		activePlayer=new PlayerProfile();
		activePlayer.loadSettings(PROFILEID);
		

		GUI.initGui("uipcfont.png");
		
		// keyboard settings
		for (int i=0; i<=10; i++) {
//			if (activePlayer.keyboardSettings[i]<0) 
				activePlayer.keyboardSettings[i]=GUI.keyboardConfigDefault[i];
			GUI.keyboardConfig[i]=activePlayer.keyboardSettings[i];
		}


		// init the sound engine
		Audio.initSounds();
		Audio.MusicVolume=activePlayer.musicVolume;
		Audio.SoundVolume=activePlayer.soundVolume;
		Audio.useMusic=activePlayer.useMusic;
		Audio.useSFX=activePlayer.useSFX;
		

		// init input system
		GameInput.initControllers();


		// generate our entity objects
		Spriter.initSpriter();
		
		// world graphics, freed and reloaded at loadWorld
		sprites[0]=new Texture(Gdx.files.internal("player.png"), true);
		
		sprites[1]=new Texture(Gdx.files.internal("tilesetcity.png"),true);
		sprites[4]=new Texture(Gdx.files.internal("m01.png"), true);
		
		
		// Light system
		isLightRendering=false;
		Light.initLights("lights.png");
		
		// objects
		Monster.initMonster(4);
		FX.initFX();
		Bullets.initBullets();
		
		
		myPlayer=new Player();
		myPlayer.init();
		
		myWorld=new World();
		
		
		// start our Splash, including a nice sound
		worldTicks=0;
		Audio.playSound(Audio.FX_SPLASH);
		GameState=INSPLASH;
	}	
	
	
	
	
	
	/* ==================
	 * Handle all interface logic (except for the gameloop)
	 * ==================
	 */
	public final void LogicLoop() {

		switch (GameState) 
		{
			case ININIT:
				init();
				break;

				
				
				
				
			case INSPLASH:
				Render.drawPaint(255, 255, 255, 255);
				Render.setAlpha(splashAlpha);
				
				if (!splashDone || splashAlpha < 255) 
				{
					splashAlpha+=32;
					if (splashAlpha > 255) splashAlpha=255;
				}

				if (!splashDone && worldTicks % 2 == 0) 
				{
					splashFrame+=16;
					if (splashFrame == 96) 
					{
						splashFrame=0;
						splashDone=true;
					}
				}

				
				// jump
				if (splashYSpeed < 6 && worldTicks % 2 == 0) splashYSpeed++;
				splashY+=splashYSpeed;
				if (splashY >= 0) 
				{
					splashY=0;
					splashYSpeed=-(splashYSpeed >> 1);
				}

				// center on screen
				tx=(Render.width >> 1) - 36;
				ty=((Render.height >> 1) - 48) + splashY;
				// render pixel
				Render.dest.set(tx, ty, tx + 72, ty + 72);
				Render.src.set(splashFrame, 0, splashFrame + 16, 16);
				Render.drawBitmap(splashImage);

				// render name
				tx=(Render.width >> 1) - 61;
				ty=(Render.height >> 1) + 30;
				Render.dest.set(tx, ty, tx + 122, ty + 26);
				Render.src.set(0, 16, 122, 42);
				Render.drawBitmap(splashImage);

				
				if (worldTicks > 48 && splashDone) 
				{

					if (mySocial != null) 
					{
						mySocial.initSocial();
						mySocial.loginSocial();
					}

					if (activePlayer.stickX[0]==-999)
					{ 
						activePlayer.resetControls(Render.width, Render.height);
					}

					
					
					// check for stored resolution
					windowedModeID=activePlayer.storedWindowedModeID;
					// not set ? (first start)
					if (windowedModeID < 0) {
						// set to a default 1080x720 and windowed
						windowedModeID=2;
						activePlayer.storedWindowedModeID=windowedModeID;
						activePlayer.saveSettings(PROFILEID);
						int setID=fetchWindowMode(windowedModeID);
						setDisplayMode(windowedModes[setID][propWinWidth], windowedModes[setID][propWinHeight], activePlayer.useFullscreen);
					}
					
					
					initNewGame();
					

					
					
					FX.addFX(FX.fGOPLUME, (Render.width>>1)-11, (Render.height>>1)-4, 0, myWorld);
					
					World.inGame=true;
					GameState=INGAME;
				}
			break;
				
		}
	}

	
	
	
	
	// [ GAME RELATED CODE ]--------------------------------------------------------------------------------
	
	/* ==================
	 * called every frame/tick 
	 * ==================
	 */
	public final void GameLoop() {
		Light.resetLights();
		Spriter.resetSpriter();

		
		handleInput();
		
		myPlayer.update(myWorld);
		

		if (World.enemyCount>=8)
		{
			myWorld.CameraTakeOverCountdown=999;
			myWorld.CameraTakeOver=true;
		}
		else if (World.enemyCount<=0)
		{
			if (myWorld.CameraTakeOver)
			{
				FX.addFX(FX.fGOPLUME, (Render.width>>1)-11, (Render.height>>1)-4, 0, myWorld);
			}
			myWorld.CameraTakeOver=false;
			World.enemyCount=0;
		}
		else
		{
			myWorld.CameraTakeOverCountdown=999;
		}


		myWorld.handleCamera(myPlayer, myPlayer);
		myWorld.update();
		
		FX.update(myWorld);
		Monster.update(myWorld,myPlayer);
		Bullets.update(myWorld,myPlayer);
		
		
		Player.addPlayerToSpriter(myPlayer, 255);
		
		
		renderScene();
	}
	
	
	
	
	
	
	/* ==================
	 * handle player input for local players
	 * ==================
	 */
	public final void handleInput() {
		
		
//		if (GameInput.controller1.isKeyboard || GameInput.controller1.isMouse) 
//		{
			if (GameInput.controller1.leftPressed) 
			{
				GameInput.controller1.leftLocked=true;
				myPlayer.leftPressed=true;
			}
			
			
			if (GameInput.controller1.rightPressed) 
			{
				GameInput.controller1.rightLocked=true;
				myPlayer.rightPressed=true;
			}
			
			
			if (GameInput.controller1.upPressed) 
			{
				GameInput.controller1.upLocked=true;
				myPlayer.upPressed=true;
			}
			
			
			if (GameInput.controller1.downPressed) 
			{
				GameInput.controller1.downLocked=true;
				myPlayer.downPressed=true;
			}
//		}
		
		
		
		
		if (GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked) myPlayer.actionPressed=true;

		if (!GameInput.controller1.isGamepad && GameInput.controller1.BUTTON_Y && !GameInput.controller1.BUTTON_Ylocked) myPlayer.jumpPressed=true;
		if (GameInput.controller1.isGamepad && GameInput.controller1.BUTTON_A && !GameInput.controller1.BUTTON_Alocked) myPlayer.jumpPressed=true;

		
		if ( (!GameInput.controller1.isGamepad && GameInput.controller1.backPressed && !GameInput.controller1.backLocked)
			|| (GameInput.controller1.isGamepad && GameInput.controller1.BUTTON_SPECIAL2 && !GameInput.controller1.BUTTON_SPECIAL2locked)) 
		{
			if (GameInput.controller1.isGamepad) GameInput.controller1.BUTTON_SPECIAL2locked=true;
			else GameInput.controller1.backLocked=true;
//			InitPauseMenu();
		}
		
		

		// Escape to quit (or B-button on gamepad)
		if (GameInput.controller1.backPressed)
		{
			Gdx.app.exit();
		}
		
	}	
	
	
	// [ OBJECT POOL CODE ]--------------------------------------------------------------------------------

	public void resetObjectPools() {
//		Monster.resetMonsters();
		FX.resetFX();
//		Bullets.resetBullets();
		Light.resetLights();
	}
	
	
	
	
	public final void renderScene() {
		Render.drawPaint(255, 0,0,0);

		
		// render floor tiles
		tx=-World.offsetX;
		for (int x=0; x < World.tileMapW>>2; x++) {
			if (tx>-96 && tx<Render.width) {
				ty=-World.offsetY;

				if (World.scene==1)
				{
					// top
					if (x%2==0)
					{
						tile=myWorld.getTileRenderMap(x, 0);
						Render.dest.set(tx, ty, tx + 64, ty + 48);
						Render.src.set(tile*64, 0, (tile*64) + 64, 48);
						Render.drawBitmap(sprites[1]);
					}
				}
				else if (World.scene==2)
				{
					if (x%4==0)
					{
						tile=myWorld.getTileRenderMap(x, 0);
						Render.dest.set(tx, ty, tx + 128, ty + 48);
						Render.src.set(tile*128, 0, (tile*128) + 128, 48);
						Render.drawBitmap(sprites[1]);
					}
					
				}
				ty+=48;
						
				// mid
				tile=myWorld.getTileRenderMap(x, 1);
				Render.dest.set(tx, ty, tx + 32, ty + 16);
				Render.src.set(tile<<5, 48, (tile<<5) + 32, 64);
				Render.drawBitmap(sprites[1]);
				ty+=16;

				// bottom
				tile=myWorld.getTileRenderMap(x, 2);
				Render.dest.set(tx, ty, tx + 32, ty + 64);
				Render.src.set(tile<<5, 64, (tile<<5) + 32, 128);
				Render.drawBitmap(sprites[1]);
				
			}
			tx+=32;
		}		
		
		
		FX.renderFX(0, sprites);
		
		Spriter.renderSpritelist(sprites);
	}
	
	
	public final void renderStatusbar() {
		tx=4;
		ty=4;
		Render.dest.set(tx,ty,tx+10,ty+11);
		Render.src.set(34,0,44,11);
		Render.drawBitmap(sprites[4]);
		
		// render stamina
		tx+=12;
		percent=((32f/myPlayer.maxStamina)*myPlayer.stamina);
		Render.dest.set(tx,ty,tx+(int)percent,ty+3);
		Render.src.set(0,4, 0+(int)percent,7);
		Render.drawBitmap(sprites[4]);
		
		ty+=4;
		// render life
		percent=((32f/myPlayer.maxLife)*myPlayer.life);
		Render.dest.set(tx,ty,tx+(int)percent,ty+5);
		Render.src.set(0,7, 0+(int)percent,13);
		Render.drawBitmap(sprites[4]);
		
		
		// player2
		if (worldTicks%50<25)
		{
			GUI.renderText("~0 to join", 0, Render.width-64, 4, 64, 0, 2);
		}
		
	
		GUI.renderText( String.format("%08d", myPlayer.score), 0, -1, 4, 200, 0, 0);
		
//		GUI.renderText("c:"+World.enemyCount,0,0,Render.height-48, Render.width,2,0);
		
//		GUI.renderText("Arrow keys to move, X to punch, Z to jump", 0, -1, (Render.height-16), Render.width-8, 2, 2);
	}
	
	
	
	public final void renderPostLights() {
		switch (GameState)
		{
			case INGAME:
				FX.renderFX(8, sprites);
				FX.renderFX(9, sprites);
				
				// letterbox bars
				Render.setARGB(255, 0, 0, 0);
				ty=(Render.height>>1)-64;
				Render.fillRect(0, 0, Render.width, ty);
				ty=(Render.height>>1)+64;
				Render.fillRect(0, ty, Render.width, Render.height-ty);

				
				renderStatusbar();
			break;
		}
		
		// raster effect
		/*
		Render.setAlpha(32);
		tx=0;
		while (tx<Render.width)
		{
			ty=0;
			while (ty<Render.height)
			{
				Render.dest.set(tx,ty,tx+64,ty+32);
				Render.src.set(150,0,214,32);
				Render.drawBitmap(sprites[4]);
				ty+=32;
			}
			tx+=64;
		}
		Render.setAlpha(255);
		*/
	}
	
	
	
	
	
	
	public final void initNewGame()
	{
		World.scene=1;
		
		
		
		myPlayer.gameInit();
		myPlayer.init();
		myWorld.newGame();
		Globals.fillRandomTable(Globals.getRandomForcedUnseeded(1000), Globals.getRandomForcedUnseeded(1000), Globals.getRandomForcedUnseeded(1000));

		
		switch (World.scene)
		{
			case 1:
				sprites[1]=new Texture(Gdx.files.internal("tilesetcity.png"),true);

				// add floor
				myWorld.putarea(0, 0, World.tileMapW, 6, 256);
				myWorld.putarea(0, 16, World.tileMapW, 4, 32);
				
				
				// generate a little scenery
				for (int x=0; x<World.tileMapW>>2; x++) {
					
					if (x%2==0)
					{
						tx2=Globals.getRandom(4);
						myWorld.putRendermap(x, 0, tx2);
						if (tx2==2)
						{
							// garage box
							myWorld.putarea((x<<2)+1, 5, 6,1,0);
							Monster.addMonster(Monster.mSCENERY, (x<<5)+5, 17, Scenery.GARAGE, myWorld);
						}
					}
					
					
					
					myWorld.putRendermap(x, 1, Globals.getRandom(2));
					myWorld.putRendermap(x, 2, 0);
					if (x%8==0)
					{
						myWorld.putRendermap(x, 2, 1);
					}
					
					if (x%7==0)
					{
						Monster.addMonster(Monster.mSCENERY, x<<4, 16, Scenery.TREE, myWorld);
						Monster.addMonster(Monster.mSCENERY, x<<4, 16, Scenery.TREETOP, myWorld);
					}
					
					if (x%3==0 && Globals.getRandom(100)>55)
					{
						Monster.addMonster(Monster.mSCENERY, x<<4, 110, Scenery.TREE, myWorld);
					}

					
					if (x%15==0)
					{
						Monster.addMonster(Monster.mSCENERY, x<<4, 51, Scenery.FIREHYDRANT, myWorld);
					}
					
					if (x%3==0 && Globals.getRandom(100)>70)
					{
						Monster.addMonster(Monster.mSCENERY, x<<4, 58, Scenery.CAR1+Globals.getRandom(3), myWorld);
					}

					if (x%3==0 && Globals.getRandom(100)>80)
					{
						Monster.addMonster(Monster.mSCENERY, x<<4, 110, Scenery.CAR1+Globals.getRandom(3), myWorld);
					}
					
				}
			break;
			
			case 2:
				sprites[1]=new Texture(Gdx.files.internal("tilesetmetro.png"),true);
				
				// add floor
				myWorld.putarea(0, 0, World.tileMapW, 6, 256);
				myWorld.putarea(0, 16, World.tileMapW, 4, 32);
				
				
				// generate a little scenery
				for (int x=0; x<World.tileMapW>>2; x++) {
					
					if (x%3==0)
					{
//						tx2=Globals.getRandom(4);
						myWorld.putRendermap(x, 0, 0);
					}
					
					
					
					myWorld.putRendermap(x, 1, Globals.getRandom(2));
					myWorld.putRendermap(x, 2, Globals.getRandom(2));
					
					
					if (x%7==0)
					{
						Monster.addMonster(Monster.mSCENERY, x<<4, 0, Scenery.SUBWAYPILAR, myWorld);
					}

				}
				
			break;
		}
		
		

		myPlayer.spawnPlayer(6, 9);
		World.offsetY=myPlayer.y-80;
		World.offsetX=myPlayer.x-(Render.width>>1);
		
		
		
		
		
		for (int i=0; i<Monster.monsterList.length; i++) {
			Monster.getMonster(i).solidify(myWorld);
		}
		
		
		for (int i=0;i<64; i++)
		{
			
			tx=Globals.getRandom(800);
			ty=(4+(Globals.getRandom(4)))<<4;
					
			if (!myWorld.isSolid(tx>>3, ty>>3, 2))
			{
				Monster.addMonster(Monster.mBRAWLER, tx, ty, Brawlers.BALD+Globals.getRandom(2), myWorld);
			}
		}
	}
	
	
}
