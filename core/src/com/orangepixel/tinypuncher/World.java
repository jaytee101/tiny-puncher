package com.orangepixel.tinypuncher;

import com.orangepixel.utils.Render;

/**
 * This contains the game world, tilemaps, etc.
 * including level generation code 
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */
public class World {

	
	public final static int tileMapW = 512; //256; // (effective: 96, rest is "Black filler" space
	public final static int tileMapH = 20; //256; 
	
	
	public final static int	UP = 0,
							RIGHT = 1,
							DOWN = 2,
							LEFT = 3;
	
	
	
	public static boolean		inGame;	// false if in level generation, interesting to know for some monsters+items 

	public static int			level;
	public static int			world;
	public static int			scene;

	public static int			score;
	public static int			coins;

	public static int			slowMoFactor=1;
	public static int 			offsetX;
	public static int 			offsetY;
	public static int			worldAge;
	public static int			worldShake;
	public static int			worldRandomValue;
	public static int			shakeHorizontal;
	public static int			shakeVertical;
	
	public static int			playerStartX;
	public static int			playerStartY;
	
	public static int			SpriteSet;
	public static int			enemyCount;
	
	public boolean					isLocalCoop;
	

	public int						lockScreen;
	public boolean					lockVertical;
	public boolean					CameraTakeOver;	// if something else besides player takes focus
	public boolean					CameraIsView;	// signal player we are back on camera-position checking
	public int						CameraTakeOverCountdown;
	public int						lockVerticalValue;
	public int						softLock;
	public boolean					autoScroll;
	public int						cameraTargetX;
	public int						cameraTargetY;

	// 8x8 tile set for the tilemap
	int[] heightMap=new int[tileMapW*tileMapH];
	
	/*
	 *  rendermap uses "block" rendering
	 *  
	 *  Buildings: (top-part) are 32x48 
	 *  Sidewalk: (2nd part) 32x32
	 *  Road: (3rd part) 32x48
	 *  
	 *  so 1/4 tileWidth is needed, and only 3 "tiles" high
	 */
	int[] renderMap=new int[(tileMapW>>2)*4];
	
	
	/* ==============
	 * World object constructor
	 * ==============
	 */
	public World() {
		lockScreen=0;
		lockVertical=false;
		lockVerticalValue=0;
		CameraTakeOver=false;
		CameraIsView=true;	// signal player we are back on camera-position checking
		softLock=0;
		autoScroll=false;
	}
	
	
	/* ==============
	 * initialise the world variables for a new game
	 * ==============
	 */
	public final void newGame() {
		level=1;

		inGame=false;
		score=0;
		coins=0;
	}
	
	/* ==============
	 * Score is game based, not player based (in case of 2 player co-op)
	 * ==============
	 */
	public final void addScore(int value, int myX, int myY) {
		score+=value;
	}
	



	
	/* ==============
	 * put specified tile at specified tile coords
	 * ==============
	 */
	public final void put(int x, int y, int tile) {
		heightMap[x+(y*tileMapW)]=tile;
	}

	/* ==============
	 * fill specified area with specified tile value
	 * ==============
	 */
	public final void putarea (int x, int y, int w, int h, int tile) {
		for (int xx=x; xx<x+w; xx++) {
			for (int yy=y; yy<y+h; yy++) {
				heightMap[xx+(yy*tileMapW)]=tile;
			}
		}
	}
	


	/* ==============
	 * put specified render tile index to rendermap at x,y coords
	 * ============== 
	 */
	public final void putRendermap(int x, int y, int tile) {
		renderMap[x+(y*(tileMapW>>2))]=tile;
	}



	/* ==============
	 * check if specified coords is sold or not (including monsters, markers, etc)
	 * ==============
	 */
	public final boolean isSolid(int tx,int ty, int height) {
		if (tx<=0 || ty<=0 || tx>=tileMapW || ty>=tileMapH || 
			(heightMap[tx+(ty*tileMapW)]>=height)) return true;
		return false;
	}
	


	/* ==============
	 * get tile from specified coords
	 * ==============
	 */
	public final int getTile(int x, int y){
		if (x>=0 && y>=0 && x<tileMapW && y<tileMapH) return heightMap[x+(y*tileMapW)];
		return -1;
	}

	/* ==============
	 * get rendermap tile from specified coords (texture)
	 * ==============
	 */
	public final int getTileRenderMap(int x, int y){
		if (x>=0 && y>=0 && x<tileMapW>>2 && y<3) return renderMap[x+(y*(tileMapW>>2))];
		return -1;
	}

	/* ==============
	 * remove all instances of specified tile from the tilemap (eg.: marker tiles after generation)
	 * ==============
	 */
	public final void clean(int tile) {
		for (int y=tileMapH; --y>=0;) {
			for (int x=tileMapW; --x>=0;) {
				if (heightMap[x+(y*tileMapW)]==tile) heightMap[x+(y*tileMapW)]=0;
			}
		}
	}
	

	


	
	

	/* ==============
	 * update world every tick, handling screenshake, movement, etc
	 * ==============
	 */
	public final void update() {
		worldAge++;
		if (worldShake>0) worldShake--;
		
		if (CameraTakeOverCountdown>0) CameraTakeOverCountdown--;
		else CameraTakeOver=false;
		
		worldRandomValue=Globals.getRandomForcedUnseeded(100);
		
		shakeVertical=0;
		shakeHorizontal=0;
		if (worldShake>0) {
			if (worldShake>24) {
				shakeVertical=Globals.getRandomForcedUnseeded(8)-4;
				shakeHorizontal=Globals.getRandomForcedUnseeded(8)-4;
			} else if (worldShake>12) {
				shakeVertical=Globals.getRandomForcedUnseeded(4)-2;
				shakeHorizontal=Globals.getRandomForcedUnseeded(4)-2;
			} else {
				shakeVertical=Globals.getRandomForcedUnseeded(2)-1;
				shakeHorizontal=Globals.getRandomForcedUnseeded(2)-1;
			}
		}
		
		offsetX+=shakeHorizontal;
		offsetY+=shakeVertical;
	
	}	

	/* ==============
	 * force a camera take over to specified coordinates 
	 * ==============
	 */
	public final void setCameraTakeOver(int myx,int myy,int duration) {
		CameraTakeOver=true;
		CameraTakeOverCountdown=duration;
		cameraTargetX=myx;
		cameraTargetY=myy;
	}
	
	
	/* ==============
	 * handle camera focussing on player 1 and in co-op mode on player1+player2
	 * ==============
	 */
	public final void handleCamera(Player tmpPlayer1, Player tmpPlayer2) {
		int tx;
		int ty;

		if (CameraTakeOver) 
		{
//			int xSpeed=((cameraTargetX- (Render.width>>1)) - offsetX) >> 3;
//			int ySpeed=((cameraTargetY- (Render.height>>1)) - offsetY) >> 3;
			
//			offsetX+=xSpeed;
//			offsetY+=ySpeed;
		} 
		else 
		{
			
			if (!isLocalCoop) 
			{
				tx=tmpPlayer1.x+8;
				ty=tmpPlayer1.y+10;
				
				int xSpeed=((tx- (Render.width>>1)) - offsetX) >> 3;
				int ySpeed=((ty- (Render.height>>1)) - offsetY) >> 3;
				
				offsetX+=xSpeed;
				offsetY+=ySpeed;
			} 
			else 
			{
				if (tmpPlayer2.Died) 
				{
					tx=tmpPlayer1.x;
					ty=tmpPlayer1.y;
				} 
				else if (tmpPlayer1.Died) 
				{
					tx=tmpPlayer2.x;
					ty=tmpPlayer2.y;
				}
				else 
				{
					if (tmpPlayer2.x>tmpPlayer1.x) 
					{
						tx=tmpPlayer1.x+((12+tmpPlayer2.x-tmpPlayer1.x)>>1);
					} 
					else 
					{
						tx=tmpPlayer2.x+((12+tmpPlayer1.x-tmpPlayer2.x)>>1);
					}
		
					if (tmpPlayer2.y>tmpPlayer1.y) 
					{
						ty=tmpPlayer1.y+((12+tmpPlayer2.y-tmpPlayer1.y)>>1);
					} 
					else 
					{
						ty=tmpPlayer2.y+((12+tmpPlayer1.y-tmpPlayer2.y)>>1);
					}
				}
				
				offsetX+=((tx- (Render.width>>1))-offsetX)>>2;
				offsetY+=((ty- (Render.height>>1))-offsetY)>>2;
			}
			
		}
		
		if (offsetX<0) 
		{
			offsetX=0;
		}
		else if (offsetX>(tileMapW<<4)-Render.width) 
		{
			offsetX=(tileMapW<<4)-Render.width;
		}
		
		
		offsetY=-((Render.height>>1)-64);
	}


	

	
	
}
