package com.orangepixel.tinypuncher;

/**
 * Used by various GUI code like dialog boxes, buttons, etc.
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class GUIListener {

	/**
	 * called when a single menu button was selected
	 */
	public void onSelected() {}

	/**
	 * called when a selection / progress bar is pressed on "decrease" side
	 */
	public void onPrevious() {}
	
	/**
	 * called when selection / progress bar is pressed on "increase" side
	 */
	public void onNext() {}
	
	public void onButtonLeft() {}
	public void onButtonMid() {}
	public void onButtonRight() {}
}

