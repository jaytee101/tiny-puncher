package com.orangepixel.tinypuncher;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

/**
 * Audio handles the intialisation , playing and stopping of sound effects and music
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */
public class Audio {

	public static boolean useSFX;
	public static boolean useMusic;
	
	public static int MusicVolume;
	public static int SoundVolume;
	public static int AmbientVolume;
	
	public static String soundFormat = ".mp3";
	public static String soundRoot = "audio/";
	
	
	// Music and sound-effects
	public static int currentMusicVolume;

	public static Music myGameMusic;
		
	public static int 	FX_SPLASH=0,
						FX_THUMP1=1,
						FX_THUMP2=2,
						FX_THUMP3=3,
						FX_THUMP4=4,
						FX_LAND1=5,
						FX_MALEOUCH1=6,
						FX_MALEOUCH2=7,
						FX_COIN=8,
						FX_WHOOSH1=9,
						FX_WHOOSH2=10,
						
						FX_MAX=11;
					
	public static String[] soundEffectFile = new String[] {
		"fxsplash",
		"fx_thump01",
		"fx_thump02",
		"fx_thumplight01",
		"fx_thumplight02",
		"fx_land01",
		"fx_maleouch1",
		"fx_maleouch2",
		"fx_coin",
		"fx_whoosh1",
		"fx_whoosh2"
	};
	
	
	public static Sound[] soundEffects = new Sound[FX_MAX];
					
	
	/* ==============
	 * initialise the sound effects, call this on startup / init of the game
	 * ==============
	 */
	public static void initSounds() {

		for (int i=0; i<soundEffectFile.length; i++)
		{
			soundEffects[i]=Gdx.audio.newSound(Gdx.files.internal(soundRoot + soundEffectFile[i]+soundFormat));
		}
		
		
//		myGameMusic = Gdx.audio.newMusic(Gdx.files.internal(root+"tune1.mp3"));
//		myGameMusic.setLooping(true);
	}
	
	

	public final static void playRandomThump() {
		switch (Globals.getRandomForcedUnseeded(4))
		{
			case 0:
				playSoundPitched(FX_THUMP1);
			break;
			
			case 1:
				playSoundPitched(FX_THUMP2);
			break;

			case 2:
				playSoundPitched(FX_THUMP3);
			break;

			case 3:
				playSoundPitched(FX_THUMP4);
			break;
			
		}
	}
	
	
	
	public final static void playRandomMaleOuch() {
		switch (Globals.getRandomForcedUnseeded(2))
		{
			case 0:
				playSoundPitched(FX_MALEOUCH1);
			break;
			
			case 1:
				playSoundPitched(FX_MALEOUCH2);
			break;
		}
	}
	
	
	
	
	public final static void playRandomWhoosh() {
		switch (Globals.getRandomForcedUnseeded(2))
		{
			case 0:
				playSoundPitched(FX_WHOOSH1);
			break;
			
			case 1:
				playSoundPitched(FX_WHOOSH2);
			break;
		}
	}	
	/* ==============
	 * play specified sound effect
	 * ==============
	 */
	public final static long playSound(int sound) {
		long soundID=-1;
		if (useSFX) 
		{
			soundID=soundEffects[sound].play(SoundVolume / 10.0f);
		}
		return soundID;
	}


	/* ==============
	 * play specified sound , randomly changes pitch slightly, to vary often used sound effects
	 * ============== 
	 */
	public final static long playSoundPitched(int sound) {
		long soundID=-1;
		
		if (useSFX) 
		{
			soundID=soundEffects[sound].play(SoundVolume / 10.0f);
			soundEffects[sound].setPitch(soundID, (88+Globals.getRandomForcedUnseeded(16))/100.0f);
		}

		return soundID;
	}
	

	
	/* ==============
	 * stop all playing sounds (music and ambience mostly)
	 * ==============
	 */
	public final static void stopAllSounds() {
		if (myGameMusic != null) myGameMusic.stop();
	}

	/* ==============
	 * start playing the background music
	 * ==============
	 */
	public final static void playBackgroundMusic() {
		if (useMusic && myGameMusic != null) 
		{
			myGameMusic.play();
			myGameMusic.setVolume(currentMusicVolume / 10.0f);
		}
	}

	/* ==============
	 * stop playing background music
	 * ==============
	 */
	public final static void stopBackgroundMusic() {
		if (useMusic) 
		{
			if (myGameMusic != null && myGameMusic.isPlaying())
				myGameMusic.pause();
		}
	}	
	

	
}
