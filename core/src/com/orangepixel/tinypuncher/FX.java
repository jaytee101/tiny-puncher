package com.orangepixel.tinypuncher;

import com.badlogic.gdx.graphics.Texture;
import com.orangepixel.utils.Render;

/**
 * Special effects, mostly visual and usually non-interacting with anything else
 * 
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class FX extends Entity {
	
	
	// FX types
	public final static int		fFLOORMARK = 0,	// includes shades
								fPUFF = 1,
								fDEBRI = 2,
								fDIGITPLUME = 3,
								fGOPLUME = 4;
	
	
	// floormark subtypes
	public final static int		floorShade = 0;
	
	
	public final static int		puffSmallSmoke = 0,
								puffBoxUpRight = 1,
								puffBoxUpLeft = 2,
								puffKickRight = 3,
								puffKickLeft = 4,
								puffLeave = 5;
	
	// debri types
	public final static int		debriBlood = 0;
	

	// movement
	public int		depth;	// 0=on floor, 16=on top of walls
	int				targetX;
	int				targetY;
	int				xSpeed;
	int				ySpeed;
	int				xSpeedInc;
	int				ySpeedInc;
	public int		renderPass;


	// looks
	public int		SpriteSet;
	int				xOffsetAdd;
	int				xOffsetMax;
	int				animDelay;
	int				animSpeed;
	
	
	// entity information
	int				myType;
	int				subType;
	
	public static FX[] fxList=new FX[2640];
	

	/* ==============
	 * initialise the object pool
	 * ==============
	 */
	public final static void initFX() {
		for (int i=fxList.length - 1; i >= 0; i--) fxList[i]=new FX();
		resetFX();
	}
	
	
	/* ==============
	 * add new FX object to the first available slot (reusing objects where deleted==true)
	 * ==============
	 */
	public final static int addFX(int mType, int aX, int aY, int mSubType, World myWorld) {
		int i=0;
		while (i<fxList.length && !fxList[i].deleted) i++;
		
		if (i<fxList.length) 
		{
			fxList[i].init(mType, aX, aY,  mSubType, myWorld);
			fxList[i].UID=Entity.getUID();
			return i;
		}
		return -1;
	}		
		
	/* ==============
	 * reset the deleted flag to true for all objects
	 * ==============
	 */
	public final static void resetFX() {
		for (int i=0; i<fxList.length; i++) fxList[i].deleted=true;
	}

	
	
	
	/* ==============
	 * initialise a new FX object
	 * ==============
	 */
	public void init(int mType, int aX,int aY, int mSubType, World myWorld) {
		// we are now concidered active monster
		deleted=false;
		died=false;
		depth=0;
		
		// generate a unique random int for each monster
		int t=(UID*mType) ^ ((aX<<UID) << 11);
		myRandom=myRandom ^ (myRandom >> 19) ^ (t ^ (t >> 8));
		
		
		renderPass=2;	// 0 = floor  1 = direct on floor   8=post walls 9=interface (ontop of lighting)
		
		subType=mSubType;
		myType=mType;
		x=aX;
		y=aY;
		xSpeed=0;
		ySpeed=0;
		xOffsetMax=0;
		xOffsetAdd=16;
		
		// defaults
		SpriteSet=4;
		alpha=255;
		rotate=0;
		aiState=0;
		visible=true;
		
		switch (myType) {
			case fFLOORMARK:
				w=10;
				h=3;
				xOffset=0;
				xOffsetMax=xOffset;
				xOffsetAdd=w;
				yOffset=0;
				animSpeed=1;
				alpha=128;
				renderPass=0;
			break;
			
			
			
			case fPUFF:
				switch (subType)
				{
					case puffSmallSmoke:
						w=8;
						h=8;
						xOffset=44;
						yOffset=0;
						xOffsetAdd=8;
						xOffsetMax=76;
						animSpeed=2;
						renderPass=0;
					break;
					
					case puffBoxUpRight:
						alpha=128;
						w=5;
						h=6;
						xOffset=120;
						yOffset=0;
						animSpeed=2;
						renderPass=8;
						ySpeed=-8;
						xSpeed=8;
					break;
					
					case puffBoxUpLeft:
						alpha=128;
						w=5;
						h=6;
						xOffset=120;
						yOffset=7;
						animSpeed=2;
						renderPass=8;
						ySpeed=-8;
						xSpeed=-8;
					break;
					
					case puffKickRight:
						alpha=128;
						w=6;
						h=4;
						xOffset=126;
						yOffset=0;
						animSpeed=2;
						renderPass=8;
						xSpeed=8;
					break;
					
					case puffKickLeft:
						alpha=128;
						w=6;
						h=4;
						xOffset=126;
						yOffset=5;
						animSpeed=2;
						renderPass=8;
						xSpeed=-8;
					break;
					
					case puffLeave:
						w=4;
						h=3;
						xOffset=133;
						yOffset=0;
						rotate=myRandom(360);
						xSpeed=(myRandom(6)-3)<<3;
						ySpeed=(1+myRandom(4))<<3;
						renderPass=8;
						animSpeed=8;
					break;
				}
			break;
			
			case fDEBRI:
				switch (subType)
				{
					case debriBlood:
						w=1;
						h=1;
						xOffset=0;
						yOffset=9+Globals.getRandom(2);
					break;
				}
				
				switch (Globals.getRandomForcedUnseeded(40)%10) {
					default:
						xSpeed=-2+Globals.getRandomForcedUnseeded(4);
						ySpeed=-6+Globals.getRandomForcedUnseeded(4);
						targetY=y+11;
					break;
					
					case 1:
						xSpeed=2+Globals.getRandomForcedUnseeded(4); 
						ySpeed=-4+Globals.getRandomForcedUnseeded(4);
						targetY=y+11;
					break;
					
					case 2:
						xSpeed=-1+Globals.getRandomForcedUnseeded(4);
						ySpeed=-3+Globals.getRandomForcedUnseeded(4);
						targetY=y+2;
					break;
					
					case 3:
						xSpeed=1+Globals.getRandomForcedUnseeded(4); 
						ySpeed=-3+Globals.getRandomForcedUnseeded(4);
						targetY=y+2;
					break;
				}				
			
				xSpeed=xSpeed<<3;
				ySpeed=ySpeed<<3;
				animSpeed=999;
			break;
			
			
			case fDIGITPLUME:
				visible=true;
				w=96;
				h=96;
				ySpeed=-8;
				animSpeed=999;
				aiCountdown=48;
				renderPass=9;
			break;
			
			
			case fGOPLUME:
				w=23;
				h=9;
				xOffset=150;
				yOffset=35;
				animSpeed=200;
				aiCountdown=200;
				renderPass=8;
			break;
		}
		
		
		floatX=x<<4;
		floatY=y<<4;
		animDelay=animSpeed;
	}

	
	/* ==============
	 * update the FX object for every tick
	 * ==============
	 */
	public final void updatefx(World myWorld) {
		
		
		// default animation
		if (animDelay>0) animDelay--;
		else {
			animDelay=animSpeed;
			
			xOffset+=xOffsetAdd;
			if (xOffset>xOffsetMax) died=true;
		}
		
		
		
		switch (myType) {
			
			case fPUFF:
				floatX+=xSpeed;
				floatY+=ySpeed;
				x=floatX>>4;
				y=floatY>>4;
			break;
				
			case fDEBRI:
				if (aiState==0) {
					floatX+=xSpeed;
					floatY+=ySpeed;
					x=floatX>>4;
					y=floatY>>4;
					
					if (ySpeed<13<<4) ySpeed+=8;
					
					if (y>=targetY) {
						aiState=999;
						if (myWorld.isSolid(x>>4, y>>4,4)) died=true;
					}
				}		
				else if (aiState==999)
				{
					alpha-=2;
					animDelay=999;
					if (alpha<=0)
					{
						alpha=0;
						died=true;
					}
				}
			break;
			
			
			
			case fDIGITPLUME:
				floatY+=ySpeed;
				y=floatY>>4;
				x=floatX>>4;
		
				if (ySpeed<0) ySpeed++;
				else
				{
					if (aiCountdown>0) aiCountdown--;
					else
					{
						died=true;
					}
					
					if (aiCountdown%8<4) visible=!visible;
				}
				
				animDelay=999;
			break;
			
			
			
			case fGOPLUME:
				if (aiCountdown>0) aiCountdown--;
				if (aiCountdown%6==0) visible=!visible;
			break;
		}
	}
	
	
	
	
	
	
	
	// update monsters and add live ones to the sprite list
	public final static void update(World myWorld) {
		int i=0;
		int tx;
		int ty;
		
		FX tmpFX;
		
		while (i<FX.fxList.length) 
		{
			tmpFX=FX.fxList[i];
			
			if (!tmpFX.deleted && !tmpFX.died) 
			{
				
				tmpFX.updatefx(myWorld);
				
				if (!tmpFX.died) 
				{
					
					if (tmpFX.renderPass!=0 && tmpFX.renderPass<8 && tmpFX.visible) 
					{
						// alive, so add to our sprite list
						
						tx=tmpFX.x-World.offsetX;
						ty=tmpFX.y-World.offsetY;
	
						Render.dest.set(tx,ty,tx+tmpFX.w, ty+tmpFX.h);
						Render.src.set(tmpFX.xOffset, tmpFX.yOffset, tmpFX.xOffset+tmpFX.w, tmpFX.yOffset+tmpFX.h);
						Spriter.addSprite(Render.dest, Render.src, tmpFX.SpriteSet, tmpFX.alpha, tmpFX.rotate, tmpFX.depth, false);
					}
					
				} 
				else 
				{
					tmpFX.deleted=true;
				}
			}
			i++;
		}
	}

	
	
	
	public final static void renderFX(int renderPass, Texture[] sprites) {
		int i=0;
		int tx;
		int ty;
		
		FX tmpFX;
		
		while (i<FX.fxList.length) {
			tmpFX=FX.fxList[i];
			if (!tmpFX.deleted && !tmpFX.died && tmpFX.renderPass==renderPass) {
				// alive, so add to our sprite list
				
				if (tmpFX.visible) {
					if (renderPass==8)
					{
						// coords are absolute to screen coords, not world coords
						tx=tmpFX.x;
						ty=tmpFX.y;
					}
					else 
					{
						tx=tmpFX.x-World.offsetX;
						ty=tmpFX.y-World.offsetY;
					}
	


					Render.setAlpha(tmpFX.alpha);

					if (tmpFX.myType==fDIGITPLUME) 
					{
						GUI.renderText(Integer.toString(tmpFX.subType), 0, tx, ty, 400, 1, 0); //6);
					}
					else
					{
						Render.dest.set(tx,ty,tx+tmpFX.w, ty+tmpFX.h);
						Render.src.set(tmpFX.xOffset, tmpFX.yOffset, tmpFX.xOffset+tmpFX.w, tmpFX.yOffset+tmpFX.h);
						Render.drawBitmap(sprites[tmpFX.SpriteSet]);
					}
				}
			}
			i++;
		}		

		Render.setAlpha(255);
	}		
	
}
