package com.orangepixel.tinypuncher;


import com.badlogic.gdx.graphics.Texture;
import com.orangepixel.utils.Rect;
import com.orangepixel.utils.Render;


/**
 * Contains all sprites rendered per frame, and sorts them from top-to-bottom (top-down view)
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class Spriter {

	Rect dest = new Rect();
	Rect src = new Rect();
	int spriteIDX;
	int spriteAlpha;
	int rotation;
	int	depth;
	
	boolean flipped;
	
	boolean drawn;
	boolean deleted;
	
	// object-pools
	static Spriter[] spriteList = new Spriter[640]; //3200];
	private static int spritesAdded;

	public Spriter() {
		deleted=true;
		drawn=true;
		spritesAdded=0;
	}
	

	
	public final static void initSpriter() {
		for (int i=spriteList.length - 1; i >= 0; i--) spriteList[i]=new Spriter();
	}
	
	public final static void resetSpriter() {
		for (int i=0; i<spriteList.length; i++) spriteList[i].deleted=true;
		spritesAdded=0;
	}	
	
	public final static void setAllDrawn(boolean value) {
		for (int j=0;j<spriteList.length; j++) spriteList[j].drawn=value;
	}
	
	
	
	public final static void addSprite(Rect mDest, Rect mSrc, int idx, int alpha, int mrotate, int depth, boolean flip) {
		int i=0;
		while (i < spriteList.length && !spriteList[i].deleted) i++;
		if (i < spriteList.length) 
		{
			spriteList[i].reinit(idx, mDest, mSrc, alpha, mrotate, depth,flip);
			spritesAdded++;
		}
	}

	
	public final static void renderSpritelist(Texture[] sprites) {
		int vLine=0;
		int topSpot=-16;
		int i;
		int ty;
		
		topSpot=-16;
		vLine=topSpot;
		
		

		while (vLine < Render.height + 64) {
			vLine=topSpot;
			topSpot=-999;
			// find all sprites at this vline
			i=0;
			while (i < spritesAdded) 
			{ 
				if (!spriteList[i].drawn && !spriteList[i].deleted) 
				{ 
					ty=spriteList[i].dest.bottom+spriteList[i].depth;
					
					if (ty <= vLine) 
					{
						Render.setAlpha(spriteList[i].spriteAlpha);
						
						if (spriteList[i].rotation != 0) 
						{
							Render.drawBitmapRotated(sprites[spriteList[i].spriteIDX], spriteList[i].src, spriteList[i].dest, spriteList[i].rotation);
						} 
						else 
						{
							Render.drawBitmap(sprites[spriteList[i].spriteIDX], spriteList[i].src, spriteList[i].dest, spriteList[i].flipped);
						}

						spriteList[i].setDrawn(true);
					} 
					else 
					{
						if (ty>0 && (ty < topSpot || topSpot == -999)) 
							topSpot=ty;
					}
				} 
				i++;
			}

			if (topSpot==-999) topSpot=vLine + 1;
		}
		
		Spriter.setAllDrawn(false);
		Render.setAlpha(255);
	}	

	
	
	
	public final static void renderSpritelistShining(Texture[] sprites, int yAxis) {
		int i;
		
		i=0;
		while (i < spritesAdded) { //spriteList.length) {
			if (!spriteList[i].drawn && !spriteList[i].deleted &&
				spriteList[i].spriteAlpha==255) { 

				Render.setAlpha(spriteList[i].spriteAlpha>>2);
					
				Render.dest.set(spriteList[i].dest);
				
				if (yAxis==-1)
				{
					yAxis=Render.dest.top+Render.dest.height;
				}
				
				Render.dest.top=yAxis+Render.dest.height;
//				Render.dest.top+=(Render.dest.height<<1)-2;
				Render.dest.height=-Render.dest.height;
				
				Render.drawBitmapRotated(sprites[spriteList[i].spriteIDX], spriteList[i].src, Render.dest, spriteList[i].rotation); //rotation);

				spriteList[i].setDrawn(true);
			} 
			i++;
		}
		
		Spriter.setAllDrawn(false);
	}	
	
	
	
	
	
	
	
	public final void setDrawn(boolean value) {
		drawn=value;
	}
	
	
	public final void doDeleted() {
		deleted=true;
		drawn=true;
	}
	
	public final void reinit(int idx, Rect mDest, Rect mSrc, int malpha, int mrotate, int mdepth, boolean flip) {
		deleted=false;
		drawn=false;
		dest.set(mDest);
		src.set(mSrc);
		spriteIDX=idx;
		spriteAlpha=malpha;
		rotation=mrotate;
		depth=mdepth;
		flipped=flip;
	}
	
}
