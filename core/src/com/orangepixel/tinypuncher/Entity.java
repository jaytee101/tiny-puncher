package com.orangepixel.tinypuncher;

/**
 * Base entity class
 * Monsters, players, FX, etc all extend this
 * 
 * has a function to give objects across the game a unique ID 
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */
public class Entity {
	public int				UID=-1;
	
	
	public int				floatX;
	public int				floatY;
	public int				floatZ;
	public int				x;
	public int				y;
	public int				z;
	public int				w;
	public int				h;
	public int				xOffset;
	public int				yOffset;
	public int				alpha;
	public int				rotate;
	public boolean			visible;
	
	public boolean			activated;
	public int				activatedDelay;
	
	public int				aiState;
	public int				aiCountdown;
	public int				movementDelay;
	public int				maxLife;
	public int				life;
	public int				maxStamina;
	public int				stamina;
	public int				fireDelay;
	public boolean			died;
	public boolean			deleted;		

	// given at world-spawn only, to fake randomness
	public int				myRandom;		

	private static int mUID=0;
	
	// give every entity a unique id (for multiplayer syncing)
	public final static int getUID() {
		int result=mUID;
		mUID++;
		return result;
	}

	
	public final static int showCurrentUID() {
		return mUID;
	}
	

	// return a "unique" random for each monster
	public int myRandom(int value) {
		int result=myRandom%value;
		
		int t=1+(myRandom ^ (myRandom << 11));
		myRandom=myRandom ^ (myRandom >> 19) ^ (t ^ (t >> 8));

		return result;
	}
	
}



