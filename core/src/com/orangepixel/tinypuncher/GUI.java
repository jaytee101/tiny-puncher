package com.orangepixel.tinypuncher;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.orangepixel.controller.GameInput;
import com.orangepixel.utils.Render;

/**
 * Simple user interface handling
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class GUI {

	// DEFAULT KEYBOARD MAPPING
	public final static int	
						keyboardConfig_left=0,
						keyboardConfig_right = 1,
						keyboardConfig_up = 2,
						keyboardConfig_down = 3,
						keyboardConfig_brawl = 4,
						keyboardConfig_jump = 5,
						keyboardConfig_cancel = 6,
						keyboardConfig_map = 7,
						keyboardConfig_weapon1 = 8,
						keyboardConfig_weapon2 = 9,
						keyboardConfig_weapon3 = 10,
						keyboardConfig_weapon4 = 11; 
						
	public static String[] keyboardConfigNames = new String[] {
			"left",
			"right",
			"up",
			"down",
			"brawl",
			"jump",
			"cancel",
			"map",
			"weapon 1",
			"weapon 2",
			"weapon 3",
			"melee"
	};
	
	public static int[]	keyboardConfig = new int[] {
			Keys.LEFT, 	
			Keys.RIGHT,
			Keys.UP,
			Keys.DOWN,
			Keys.X,
			Keys.Z,
			Keys.ESCAPE,
			Keys.M,
			Keys.NUM_1,
			Keys.NUM_2,
			Keys.NUM_3,
			Keys.NUM_4
	};
	
	public static int[]	keyboardConfigDefault = new int[] {
			Keys.LEFT, 	
			Keys.RIGHT,
			Keys.UP,
			Keys.DOWN,
			Keys.X,
			Keys.Z,
			Keys.ESCAPE,
			Keys.M,
			Keys.NUM_1,
			Keys.NUM_2,
			Keys.NUM_3,
			Keys.NUM_4
	};	
	
	
	
	public static Texture 	guiImage;

	
	public static int		menuSelectedItem;
	public static int		menuSelectedItem2;
	public static String	menuSelectionTitle;

	
	
	/* ==================
	 * initialise the gui, by setting the texture to use
	 * ==================
	 */
	public final static void initGui(String fileName) {
		guiImage=new Texture(Gdx.files.internal(fileName), true);
	}
	
	
	
	
	
	
	/* ==================
	 * renders a progress bar (eg: volume select)
	 * ==================
	 */
	private final static void renderBar(int myX, int myY, int value, int maxvalue) {
		Render.setARGB(255,174,179,180);
		float percent=((62f / maxvalue) * value);
		GUI.renderText("<", 0, myX, myY, 16, 0, 0);
		Render.fillRect(myX + 6, myY, (int) percent, 5);
		GUI.renderText(">", 0, myX + 69, myY, 16, 0, 0);
	}
	
	
	
	/* ==================
	 * render a button with the name of a key on it
	 * see renderText()
	 * ==================
	 */
	private final static int renderButton(int myx, int myy, int keyboardIDX, boolean renderBackground) {
		if (guiImage==null) {
			Globals.debug("gui Image not initialised, make sure to call initGui()");
			return -1;
		}
		
		// 3letter buttons
		if (keyboardConfig[keyboardIDX]==Keys.ESCAPE || keyboardConfig[keyboardIDX]==Keys.TAB) {
			if (renderBackground) {
				Render.dest.set(myx, myy - 1, myx + 18, myy + 8);
				Render.src.set(160,23,178,32);
				Render.drawBitmap(guiImage);
			}
			
			switch (keyboardConfig[keyboardIDX]) {
				case Keys.ESCAPE:
					Render.dest.set(myx, myy - 1, myx + 18, myy + 8);
					Render.src.set(60,23,78,32);
					Render.drawBitmap(guiImage);
				break;

				case Keys.TAB:
					Render.dest.set(myx, myy - 1, myx + 18, myy + 8);
					Render.src.set(100,23,118,32);
					Render.drawBitmap(guiImage);
				break;
				
				case Keys.ENTER:
										
				break;
			}
			// warning: increase global value TX
			return 9;
		} else if (keyboardConfig[keyboardIDX]==Keys.ENTER) {
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			Render.dest.set(myx+1, myy+1, myx+7, myy+8);
			Render.src.set(152, 25, 158, 32);
			Render.drawBitmap(guiImage);	
		} else if (keyboardConfig[keyboardIDX]==Keys.LEFT) {
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			Render.dest.set(myx+3, myy+1, myx+7, myy+6);
			Render.src.set(129, 25, 133, 30);
			Render.drawBitmap(guiImage);				
		} else if (keyboardConfig[keyboardIDX]==Keys.RIGHT) {
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			Render.dest.set(myx+3, myy+1, myx+7, myy+6);
			Render.src.set(135, 25, 139, 30);
			Render.drawBitmap(guiImage);				
		} else if (keyboardConfig[keyboardIDX]==Keys.UP) {
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			Render.dest.set(myx+2, myy+2, myx+7, myy+6);
			Render.src.set(179, 23, 184, 27);
			Render.drawBitmap(guiImage);				
		} else if (keyboardConfig[keyboardIDX]==Keys.DOWN) {
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			Render.dest.set(myx+2, myy+2, myx+7, myy+6);
			Render.src.set(179, 28, 184, 32);
			Render.drawBitmap(guiImage);		
		} else if (keyboardConfig[keyboardIDX]>=Keys.F1 && keyboardConfig[keyboardIDX]<=Keys.F12) {
			// F
			Render.dest.set(myx+2, myy+1, myx+7, myy+8);
			Render.src.set(30,16, 35,16+7);
			Render.drawBitmap(guiImage);
			
			// #
			int CharValue=keyboardConfig[keyboardIDX]-Keys.F1;
			Render.dest.set(myx+8, myy+1, myx+14, myy+8);
			Render.src.set(162+(CharValue*6), 16, 167+(CharValue*6), 16+7);
			Render.drawBitmap(guiImage);
			return 9;
			
		} else {
			// key-name
			int CharValue=(Keys.toString( keyboardConfig[keyboardIDX] ).charAt(0));
			if (renderBackground) {
				Render.dest.set(myx, myy-1, myx+9, myy+8);
				Render.src.set(40, 23, 49, 32);
				Render.drawBitmap(guiImage);
			}
			
			if (CharValue>=65 && CharValue<=91) {
				CharValue-=65;
				// letters
				Render.dest.set(myx+2, myy+1, myx+7, myy+8);
				Render.src.set(CharValue*6, 16, (CharValue*6)+5, 16+7);
				Render.drawBitmap(guiImage);
			} else if (CharValue>=48 && CharValue<=58) {
				// digits
				CharValue-=48;
				Render.dest.set(myx+2, myy+1, myx+7, myy+8);
				Render.src.set(156+(CharValue*6), 16, 161+(CharValue*6), 16+7);
				Render.drawBitmap(guiImage);
			}
		}
		
		return 0;
	}	
	
	
	public final static int calculateWidth(String myText, int fontID) {
		String convertedText=myText.toUpperCase();
		char[] myChars=convertedText.toCharArray();

		int fontWidth=5;
		if (fontID==3) fontWidth=7;

		
		int targetWidth=0;
		
		if (fontID>1) {
			// count "Spaces"
			int ty2=0;
			for (int i=0; i < myChars.length; i++) {
				if ((int) myChars[i] == 32) ty2++;
			}

			targetWidth+=( ((myChars.length-ty2) * fontWidth) );
			targetWidth+=ty2;
		} else {
			targetWidth=(myChars.length * fontWidth);
		}

		if (myText.indexOf("~") > 0)
			targetWidth+=fontWidth;

		return targetWidth;
	}
	
	
	/* ==================
	 * render specified text in one of the specified pixel fonts in the GUI texture
	 * 
	 * special codes are created using ~ (~0 for rendering a xbox style "[A]".. see code for more)
	 * use | as line-break
	 * use ^0-4 to change font during rendering
	 * ==================
	 */
	public final static void renderText(String myText, int startCharacter, int myX, int myY, int myWidth, int myLineCount, int fontID) {
		if (guiImage==null) {
			Globals.debug("gui Image not initialised, make sure to call initGui()");
			return;
		}

		String convertedText=myText.toUpperCase();
		char[] myChars=convertedText.toCharArray();

		int tx=myX;
		int ty=myY;
		int ty2;
		int CharValue;
		int tLineCount=0;
		int wordLength;
		int wordID;

		int yOffset=0;
		int fontWidth=5;
		int fontWidthSprite=6;
		yOffset=(fontID * 8);
		if (fontID==3) {
			yOffset=32;
			fontWidth=7;
			fontWidthSprite=7;
		}

		// center text?
		if (tx == -1) {
			if (fontID>1) {
				// count "Spaces"
				ty2=0;
				for (int i=0; i < myChars.length; i++) {
					if ((int) myChars[i] == 32) ty2++;
				}

				tx=(Render.width >> 1) - ( ((myChars.length-ty2) * fontWidth) >>1);
				tx-=ty2;
			} else {
				tx=(Render.width >> 1) - (myChars.length * 2); // *2 = half
																// width of 1
																// character
			}

			if (myText.indexOf("~") > 0)
				tx+=4;

			myX=tx;
			// Gdx.app.log("opdebug","render text, displayW:"+displayW+"  / tx:"+tx);
		}

		int i=startCharacter;

		while (i < myChars.length) {
			CharValue=(int) myChars[i];

			// detect if next word fits in this space
			wordLength=0;
			wordID=i;
			while ((int) myChars[wordID] != 32 && wordID < myChars.length - 1) {
				wordLength++;
				wordID++;
			}

			if (tx + (wordLength*fontWidth) > myX + myWidth) {
				tx=myX;
				ty+=8;
				tLineCount++;
				if (tLineCount >= myLineCount) {
					return;
				}
			}

			switch (CharValue) {
				case 32:// space
					tx+=3;
					break;

				case 33: // !
					tx++;
					Render.dest.set(tx, ty, tx + 3, ty + 7);
					Render.src.set(247, yOffset, 250, yOffset + 7);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 39: // '
					tx++;
					Render.dest.set(tx, ty, tx + 4, ty + 4);
					Render.src.set(236, yOffset, 240, yOffset + 4);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 40: // (
					tx++;
					Render.dest.set(tx, ty, tx + 2, ty + 6);
					Render.src.set(223, yOffset, 225, yOffset + 6);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 41: // )
					tx++;
					Render.dest.set(tx, ty, tx + 2, ty + 6);
					Render.src.set(228, yOffset, 230, yOffset + 6);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 43: // +
					tx++;
					Render.dest.set(tx, ty + 2, tx + 3, ty + 5);
					Render.src.set(79, 25, 82, 28);
					Render.drawBitmap(guiImage);
					tx+=4;
					break;

				case 45: // -
					tx++;
					Render.dest.set(tx, ty + 3, tx + 3, ty + 4);
					Render.src.set(79, 26, 82, 27);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 46: // .
					tx++;
					Render.dest.set(tx, ty, tx + 3, ty + 7);
					Render.src.set(232, yOffset, 235, yOffset + 7);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 47: // /
					tx++;
					Render.dest.set(tx, ty + 1, tx + 3, ty + 6);
					Render.src.set(84, 24, 87, 29);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 58: // :
					tx++;
					Render.dest.set(tx, ty, tx + 3, ty + 7);
					Render.src.set(250, yOffset, 253, yOffset + 7);
					Render.drawBitmap(guiImage);
					tx+=3;
					break;

				case 60: // <
					Render.dest.set(tx, ty, tx + 4, ty + 7);
					Render.src.set(129, 24, 133, 31);
					Render.drawBitmap(guiImage);
					tx+=5;
					break;

				case 62: // >
					Render.dest.set(tx, ty, tx + 4, ty + 7);
					Render.src.set(135, 24, 139, 31);
					Render.drawBitmap(guiImage);
					tx+=5;
					break;

				case 63: // ?
					tx++;
					Render.dest.set(tx, ty, tx + 5, ty + 7);
					Render.src.set(241, yOffset, 246, yOffset + 7);
					Render.drawBitmap(guiImage);
					tx+=5;
					break;

				case 94: // ^
					i++;
					CharValue=(int)myChars[i];
					fontID=CharValue-48;
					
					yOffset=(fontID * 8);
					if (fontID==3) {
						yOffset=32;
						fontWidth=7;
						fontWidthSprite=7;
					}					
					break;
					
				case 124: // | used as line-break
					tx=myX;
					ty+=7;
					tLineCount++;
					if (tLineCount >= myLineCount) {
						return;
					}
					break;

				case 126: // used for button/key images
					i++;
					CharValue=(int) myChars[i];
					switch (CharValue - 48) {
						case 0: // gamepad-A
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(0, 23, 9, 32);
							Render.drawBitmap(guiImage);
							break;

						case 1: // gamepad-B
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(10, 23, 19, 32);
							Render.drawBitmap(guiImage);
							break;

						case 2: // gamepad-X
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(20, 23, 29, 32);
							Render.drawBitmap(guiImage);
							break;

						case 3: // gamepad-Y
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(30, 23, 39, 32);
							Render.drawBitmap(guiImage);
							break;

						case 4: // keyboard-X
							tx+=renderButton(tx,ty,keyboardConfig_brawl,true);
							break;

						case 5: // keyboard-I
							tx+=renderButton(tx,ty,keyboardConfig_jump,true);
							break;

						case 6: // keyboard-ESC
							tx+=renderButton(tx,ty,keyboardConfig_cancel,true);
							break;

						case 7: // keyboard-M
							tx+=renderButton(tx,ty,keyboardConfig_map,true);
							break;

						case 8: // keyboard-TAB
							tx+=renderButton(tx,ty,keyboardConfig_map,true);
							break;

						case 9: // xbox |> start
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(118, 23, 127, 31);
							Render.drawBitmap(guiImage);
							break;

						case 20: // ~d = DPAD / arrow
							Render.dest.set(tx, ty - 1, tx + 9, ty + 8);
							Render.src.set(141, 23, 150, 32);
							Render.drawBitmap(guiImage);
							break;

					}
					tx+=10;
					break;

				default:
					if (CharValue > 64 && CharValue < 91) {
						// letters
						CharValue-=65;

						if (CharValue==12 || CharValue==22) tx++;
						Render.dest.set(tx, ty, tx+fontWidth, ty + 7);
						Render.src.set(CharValue*fontWidthSprite, yOffset, (CharValue*fontWidthSprite)+fontWidth, yOffset + 7);
						Render.drawBitmap(guiImage);
						if (CharValue==12 || CharValue==22) tx++;

						if (fontID==3) tx+=fontWidth;
						else if (fontID==2) tx+=6;
						else tx+=4;
					} else if (CharValue > 47 && CharValue < 58) {
						// numbers
						CharValue-=48;
						
						Render.dest.set(tx, ty, tx+fontWidth, ty + 7);
						Render.src.set(156 + (CharValue*fontWidthSprite), yOffset, 156+(CharValue*fontWidthSprite)+fontWidth, yOffset + 7);
						Render.drawBitmap(guiImage);

						if (fontID==3) tx+=fontWidth;
						else if (fontID == 2) tx+=6;
						else tx+=4;
					} else {
						// unidentified char
					}
					break;
			}

			i++;
		}
	}

	
	
	
	
	/* ==================
	 * render the horizontal menu bar behind menu items
	 * ==================
	 */
	private final static void renderMenuBar(int myX, int myY, int myW, boolean isSelected) {
		if (isSelected) 
		{
			Render.setAlpha(255);
			Render.dest.set(myX,myY-1,myX+myW,myY+9);
			if (myW==160)
			{
				Render.src.set(0,90,160,100);
			}
			else 
			{
				Render.src.set(85,79,160,89);
			}
			Render.drawBitmap(guiImage);
		} 
		else 
		{
			Render.setAlpha(90);
			Render.dest.set(myX,myY-1,myX+myW,myY+9);
			if (myW==160)
			{
				Render.src.set(0,90,160,100);
			}
			else
			{
				Render.src.set(85,79,160,89);
			}
			Render.drawBitmap(guiImage);
		}
		Render.setAlpha(255);
	}

	
	/* ==================
	 * render thin menu option bar, and handle touch+mouse hovering
	 * myListener.onSelected() is called when this menu option is selected by the user
	 * ==================
	 */ 
	public final static void renderMenuOptionThin(Texture image,String title, int myX, int mY, GUIListener myListener) {
		renderMenuBar(myX,mY, 160, (menuSelectedItem==menuSelectedItem2) );
		GUI.renderText(title, 0, myX+4, mY, Render.width, 1, 0);
		
		// touch controls
		if (GameInput.touchReleased && GameInput.touchX >= myX && GameInput.touchX <= myX + 80 && GameInput.touchY >= mY - 4 && GameInput.touchY <= mY + 14 && GUI.menuSelectedItem != GUI.menuSelectedItem2) {
			GameInput.touchReleased=false;
			GUI.menuSelectedItem=GUI.menuSelectedItem2;
		}

		// mouse controls
		if (GameInput.controller1.isMouse && GameInput.cursorX >= myX && GameInput.cursorX <= myX + 80 && GameInput.cursorY >= mY && GameInput.cursorY <= mY + 14) {
			if (GUI.menuSelectedItem != GUI.menuSelectedItem2) {
				GUI.menuSelectedItem=GUI.menuSelectedItem2;
			}
		} else if (GameInput.controller1.isMouse && GUI.menuSelectedItem == GUI.menuSelectedItem2) {
			GUI.menuSelectedItem=-1;
		}		
		
		if (GUI.menuSelectedItem == GUI.menuSelectedItem2) {
			if ((GameInput.controller1.isGamepad && GameInput.controller1.BUTTON_A && !GameInput.controller1.BUTTON_Alocked) 
				|| (!GameInput.controller1.isGamepad && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked)
				|| (GameInput.controller1.isMouse && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked) 
				|| (GameInput.controller1.isTouchscreen)) {

				if (GameInput.controller1.BUTTON_X) GameInput.controller1.BUTTON_Xlocked=true;
				else GameInput.controller1.BUTTON_Alocked=true;

				GUI.menuSelectedItem=0;
				myListener.onSelected();
			}
		}
		
		GUI.menuSelectedItem2++;		
	}

	
	/* ==================
	 * render menu bar with progress in it (for example rendering sound-volume)
	 * myListener.onPrevious() and onNext() are called for less / more (- / +)
	 * ==================
	 */
	public final static void renderMenuOptionProgress(String title, int myX, int myY, int Value, int MaxValue, GUIListener myListener) {
		GUI.renderMenuBar(myX, myY, 160, (GUI.menuSelectedItem==GUI.menuSelectedItem2) );
		GUI.renderText(title, 0, myX+4, myY, Render.width, 1, 0);
		GUI.renderBar((Render.width >> 1), myY + 2, Value,MaxValue);
		
		// touch controls
		if (GameInput.controller1.isTouchscreen && GameInput.touchReleased && GameInput.touchX >= myX && GameInput.touchX <= myX + 160 && GameInput.touchY >= myY && GameInput.touchY <= myY + 16) {
			GameInput.touchReleased=false;
			if (GUI.menuSelectedItem != GUI.menuSelectedItem2) {
				GUI.menuSelectedItem=GUI.menuSelectedItem2;
			}
			
			if (GameInput.touchX >= myX + 78 && GameInput.touchX <= myX + 100) GameInput.controller1.leftPressed=true;

			if (GameInput.touchX >= myX + 140 && GameInput.touchX <= myX + 160) GameInput.controller1.rightPressed=true;
		} else if (GameInput.controller1.isTouchscreen){
			GameInput.controller1.leftPressed=false;
			GameInput.controller1.leftLocked=false;
			GameInput.controller1.rightPressed=false;
			GameInput.controller1.rightLocked=false;
		}

		// mouse controls
		if (GameInput.controller1.isMouse) {
			if (GameInput.cursorX >= myX && GameInput.cursorX <= myX + 160 && GameInput.cursorY >= myY && GameInput.cursorY <= myY + 10) {
				if (GUI.menuSelectedItem != GUI.menuSelectedItem2) {
					GUI.menuSelectedItem=GUI.menuSelectedItem2;
				}

				if (GameInput.cursorX >= myX + 78 && GameInput.cursorX <= myX + 100 && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked) {
					GameInput.controller1.BUTTON_Xlocked=true;
					GameInput.controller1.leftPressed=true;
				} else {
					GameInput.controller1.leftPressed=false;
					GameInput.controller1.leftLocked=false;
				}

				if (GameInput.cursorX >= myX + 140 && GameInput.cursorX <= myX + 160 && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked) {
					GameInput.controller1.BUTTON_Xlocked=true;
					GameInput.controller1.rightPressed=true;
				} else {
					GameInput.controller1.rightPressed=false;
					GameInput.controller1.rightLocked=false;
				}
			}

		} else if (GameInput.controller1.isMouse && GUI.menuSelectedItem == GUI.menuSelectedItem2) {
			GUI.menuSelectedItem=-1;
		}


		// up/down volume
		if (GUI.menuSelectedItem == GUI.menuSelectedItem2) {
			if (GameInput.controller1.leftPressed && !GameInput.controller1.leftLocked && Audio.MusicVolume > 0) {
				GameInput.controller1.leftLocked=true;

				myListener.onPrevious();
			}

			if (GameInput.controller1.rightPressed && !GameInput.controller1.rightLocked && Audio.MusicVolume < 10) {
				GameInput.controller1.rightLocked=true;
				
				myListener.onNext();
			}
		}

		GUI.menuSelectedItem2++;
	}


	
	/* ==================
	 * Render a menu bar with selection in it (for example: resolution picker)
	 * onPrevious() and onNext() are called on the listener when < or > is clicked
	 * ==================
	 */
	public final static void renderMenuOptionSelector(String title,int myX, int myY, GUIListener myListener) {

		GUI.renderMenuBar(myX, myY, 160, (GUI.menuSelectedItem==GUI.menuSelectedItem2) );
		GUI.renderText(title, 0, myX+4, myY, Render.width, 1, 0);
		
		GUI.renderText("<   "+menuSelectionTitle, 0, (Render.width >> 1), myY, Render.width, 1, 0);
		GUI.renderText(">", 0, (Render.width >> 1) + 69, myY, Render.width, 1, 0);
		
		// touch controls
		if (GameInput.touchReleased && GameInput.touchX >= myX && GameInput.touchX <= myX + 80 && GameInput.touchY >= myY && GameInput.touchY <= myY + 10 && GUI.menuSelectedItem != GUI.menuSelectedItem2) {
			GameInput.touchReleased=false;
			GUI.menuSelectedItem=GUI.menuSelectedItem2;
		}

		// mouse controls
		if (GameInput.controller1.isMouse) {
			if (GameInput.cursorX >= myX && GameInput.cursorX <= myX + 160 && GameInput.cursorY >= myY && GameInput.cursorY <= myY + 10) {
				if (GUI.menuSelectedItem != GUI.menuSelectedItem2) {
					GUI.menuSelectedItem=GUI.menuSelectedItem2;
				}

				if (GameInput.cursorX >= myX + 78 && GameInput.cursorX <= myX + 100 && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked) {
					GameInput.controller1.BUTTON_Xlocked=true;
					GameInput.controller1.leftPressed=true;
				} else {
					GameInput.controller1.leftPressed=false;
					GameInput.controller1.leftLocked=false;
				}

				if (GameInput.cursorX >= myX + 140 && GameInput.cursorX <= myX + 160 && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked) {
					GameInput.controller1.BUTTON_Xlocked=true;
					GameInput.controller1.rightPressed=true;
				} else {
					GameInput.controller1.rightPressed=false;
					GameInput.controller1.rightLocked=false;
				}
			}

		} else if (GameInput.controller1.isMouse && GUI.menuSelectedItem == GUI.menuSelectedItem2) {
			GUI.menuSelectedItem=-1;
		}		

		
		
		if (GUI.menuSelectedItem == GUI.menuSelectedItem2) {
			if (GameInput.controller1.leftPressed && !GameInput.controller1.leftLocked) {
				GameInput.controller1.leftLocked=true;
				myListener.onPrevious();
			}

			if (GameInput.controller1.rightPressed && !GameInput.controller1.rightLocked) {
				GameInput.controller1.rightLocked=true;
				myListener.onNext();
			}
		}		
		
	
		GUI.menuSelectedItem2++;
	}
	
	
	
	/* ==================
	 * used only for keyboard input-setup (renders specified key name onto a button)
	 * ==================
	 */
	public final static void renderMenuOptionInputSetup(int myX, int mY, int keyIDX) {
		
		renderMenuBar(myX,mY, 75, (menuSelectedItem==menuSelectedItem2) );

		if (keyIDX<0) {
			GUI.renderText( "reset default", 0, myX+4, mY, Render.width, 1, 0);
		} else {
			GUI.renderText( GUI.keyboardConfigNames[keyIDX], 0, myX+4, mY, Render.width, 1, 0);
			GUI.renderButton(myX+55, mY, keyIDX , false);
		}
	}
	
	
	
	
	/* ==================
	 * render a simple menu button with text
	 * listener onSelect() is called when selected by user
	 * ==================
	 */
	public final static void renderMenuButton(String title, int myX, int myY, GUIListener myListener) {
		
		if (GameInput.controller1.isMouse && GameInput.cursorX>=myX && GameInput.cursorY>=myY && GameInput.cursorX<=myX+80 && GameInput.cursorY<=myY+16) { 
			menuSelectedItem=menuSelectedItem2;
		} else if (GameInput.controller1.isMouse && menuSelectedItem==menuSelectedItem2) menuSelectedItem=-1;

		
		
		if (GameInput.controller1.isTouchscreen && GameInput.touchX>=myX && GameInput.touchY>=myY && GameInput.touchX<=myX+80 && GameInput.touchY<=myY+16) {
			menuSelectedItem=menuSelectedItem2;
		}
		
		
		
		if (menuSelectedItem2==menuSelectedItem) {
			Render.dest.set(myX,myY,myX+78,myY+16);
			Render.src.set(0,56,78,72);
			Render.drawBitmap(guiImage);
			renderText(title,0,myX+6,myY+5,78,0,2);
		} else {
			Render.dest.set(myX,myY,myX+78,myY+16);
			Render.src.set(0,73,78,89);
			Render.drawBitmap(guiImage);
			
			Render.setAlpha(200);
			renderText(title,0,myX+6,myY+5,78,0,2);
			Render.setAlpha(255);
		}
		
		
		if (menuSelectedItem2==menuSelectedItem)
		{
			if ((GameInput.controller1.isGamepad && GameInput.controller1.BUTTON_A && !GameInput.controller1.BUTTON_Alocked)
					|| (GameInput.controller1.isTouchscreen && GameInput.touchReleased)
					|| (!GameInput.controller1.isGamepad && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked)) {
					
					if (GameInput.controller1.isGamepad) GameInput.controller1.BUTTON_Alocked=true;
					else if (GameInput.controller1.isTouchscreen) GameInput.touchReleased=false;
					else GameInput.controller1.BUTTON_Xlocked=true;
					
					myListener.onSelected();
					return;
			}
		}
		
		menuSelectedItem2++;
	}	
	
	
	
	/* ==================
	 * call this after rendering all menu options, it handles the up/down browsing through items
	 * ==================
	 */
	public final static void handleMenuSelection() 
	{
		GUI.menuSelectedItem2--;
		
		if (GameInput.controller1.upPressed && !GameInput.controller1.upLocked) {
			GameInput.controller1.upLocked=true;
			if (GUI.menuSelectedItem>0) GUI.menuSelectedItem--;
		}
		
		if (GameInput.controller1.downPressed && !GameInput.controller1.downLocked) {
			GameInput.controller1.downLocked=true;
			if (GUI.menuSelectedItem<GUI.menuSelectedItem2) GUI.menuSelectedItem++;
		}						
	}
	
		
	

	/* ==================
	 * dialog rendering 
	 * ==================
	 */
	public static int buttonHover=-1;
	
	public final static void renderDialog(String message, int myX, int myY, String buttonLeft, String buttonMid, String buttonRight, GUIListener myListener) {
		Render.dest.set(myX,myY,myX+158, myY+48);
		Render.src.set(0,171, 158,219);
		Render.drawBitmap(guiImage);
		
		GUI.renderText( message, 0, myX+4, myY+10, 152, 4, 0);
		
		if (GUI.buttonHover<0)
		{
			if (!GameInput.controller1.BUTTON_X) GUI.buttonHover=0;
		}
		else
		{
			
			if (GameInput.controller1.leftPressed && !GameInput.controller1.leftLocked)
			{
				buttonHover--;
				if (buttonHover<0) buttonHover=0;
				if (buttonHover==1 && (buttonMid==null || buttonMid=="")) buttonHover--;
			}
			
			if (GameInput.controller1.rightPressed && !GameInput.controller1.rightLocked)
			{
				buttonHover++;
				if (buttonHover==1 && (buttonMid==null || buttonMid=="")) buttonHover++;
				if (buttonHover>2) buttonHover=2;
			}
		}
		
		
		// buttons
		int ty=myY+35;
		int tx=(myX+79)- 54;
		if (buttonLeft!=null && buttonLeft!="") {
			if (GameInput.controller1.isMouse && GameInput.cursorX >= tx && GameInput.cursorX <= tx + 30 && GameInput.cursorY >= ty && GameInput.cursorY <= ty + 14) {
				if (buttonHover != 0) {
					buttonHover=0;
//					Audio.playSound(Audio.FX_UISELECT);
				}
			} else {
				if (GameInput.controller1.isMouse && buttonHover == 0) buttonHover=-1;
			}

			if (buttonHover == 0) {
				Render.dest.set(tx, ty, tx + 31, ty + 10);
				Render.src.set(0, 221, 31, 231);
				Render.drawBitmap(guiImage);
			} else {
				Render.dest.set(tx, ty, tx + 31, ty + 10);
				Render.src.set(32, 221, 63, 231);
				Render.drawBitmap(guiImage);
			}
			GUI.renderText(buttonLeft, 0, tx+8, ty+2, 29, 0, 0);
		} else {
			if (buttonHover==0) buttonHover=1;
		}			
		// Activate/select inventory item?
		if (buttonHover==0 &&
				((GameInput.controller1.isKeyboard && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked)
				|| (GameInput.controller1.isTouchscreen && GameInput.touchReleased && GameInput.touchX >= tx && GameInput.touchY >= ty - 8 && GameInput.touchX <= tx + 30 && GameInput.touchY <= ty + 20) 
				|| (GameInput.controller1.isMouse && GameInput.cursorX >= tx && GameInput.cursorX <= tx + 30 && GameInput.cursorY >= ty - 8 && GameInput.cursorY <= ty + 20 && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked)) 
			) 
		{
			if (GameInput.controller1.BUTTON_X) GameInput.controller1.BUTTON_Xlocked=true;
			else if (GameInput.controller1.isTouchscreen) GameInput.touchReleased=false;
			else GameInput.controller1.BUTTON_Alocked=true;

//			Audio.playSound(Audio.FX_UISELECT);		
			myListener.onButtonLeft();
		}		
		
		
		
		
		// middle button
		tx=(Render.width >> 1) - 15;
		if (buttonMid!=null && buttonMid!="") {
			if (GameInput.controller1.isMouse && GameInput.cursorX >= tx && GameInput.cursorX <= tx + 30 && GameInput.cursorY >= ty && GameInput.cursorY <= ty + 14) {
				if (buttonHover != 1) {
					buttonHover=1;
//					Audio.playSound(Audio.FX_UISELECT);
				}
			} else {
				if (GameInput.controller1.isMouse && buttonHover == 1) buttonHover=-1;
			}

			if (buttonHover == 1) {
				Render.dest.set(tx, ty, tx + 31, ty + 10);
				Render.src.set(0, 221, 31, 231);
				Render.drawBitmap(guiImage);
			} else {
				Render.dest.set(tx, ty, tx + 31, ty + 10);
				Render.src.set(32, 221, 63, 231);
				Render.drawBitmap(guiImage);
			}
			GUI.renderText(buttonMid, 0, tx + 8, ty + 1, 29, 0, 0);
		}				
		
		// Activate/select inventory item?
		if (buttonHover==1 &&
				((GameInput.controller1.isKeyboard && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked)
				|| (GameInput.controller1.isTouchscreen && GameInput.touchReleased && GameInput.touchX >= tx && GameInput.touchY >= ty - 8 && GameInput.touchX <= tx + 30 && GameInput.touchY <= ty + 20) 
				|| (GameInput.controller1.isMouse && GameInput.cursorX >= tx && GameInput.cursorX <= tx + 30 && GameInput.cursorY >= ty - 8 && GameInput.cursorY <= ty + 20 && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked)) 
			) 
		{
			if (GameInput.controller1.BUTTON_X) GameInput.controller1.BUTTON_Xlocked=true;
			else if (GameInput.controller1.isTouchscreen) GameInput.touchReleased=false;
			else GameInput.controller1.BUTTON_Alocked=true;

//			Audio.playSound(Audio.FX_UISELECT);		
			myListener.onButtonMid();
		}				
		
		
		// right button
		tx=(Render.width >> 1) + 25;
		if (buttonRight!=null && buttonRight!="") {
			if (GameInput.controller1.isMouse && GameInput.cursorX >= tx && GameInput.cursorX <= tx + 30 && GameInput.cursorY >= ty && GameInput.cursorY <= ty + 14) {
				if (buttonHover != 2) {
					buttonHover=2;
//					Audio.playSound(Audio.FX_UISELECT);
				}
			} else {
				if (GameInput.controller1.isMouse && buttonHover == 2) buttonHover=-1;
			}

			if (buttonHover == 2) {
				Render.dest.set(tx, ty, tx + 31, ty + 10);
				Render.src.set(0, 221, 31, 231);
				Render.drawBitmap(guiImage);
			} else {
				Render.dest.set(tx, ty, tx + 31, ty + 10);
				Render.src.set(32, 221, 63, 231);
				Render.drawBitmap(guiImage);
			}
			GUI.renderText(buttonRight, 0, tx + 8, ty + 1, 29, 0, 0);
		}				
		
		if (GameInput.controller1.backPressed && !GameInput.controller1.backLocked) 
		{
			GameInput.controller1.backLocked=true;
			myListener.onButtonRight();
		}
		
		// Activate/select inventory item?
		if (buttonHover==2 &&
				((GameInput.controller1.isKeyboard && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked)
				|| (GameInput.controller1.isTouchscreen && GameInput.touchReleased && GameInput.touchX >= tx && GameInput.touchY >= ty - 8 && GameInput.touchX <= tx + 30 && GameInput.touchY <= ty + 20) 
				|| (GameInput.controller1.isMouse && GameInput.cursorX >= tx && GameInput.cursorX <= tx + 30 && GameInput.cursorY >= ty - 8 && GameInput.cursorY <= ty + 20 && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked)) 
			) 
		{

			if (GameInput.controller1.BUTTON_X) GameInput.controller1.BUTTON_Xlocked=true;
			else if (GameInput.controller1.isTouchscreen) GameInput.touchReleased=false;
			else GameInput.controller1.BUTTON_Alocked=true;

//			Audio.playSound(Audio.FX_UISELECT);		
			myListener.onButtonRight();
		}			
		
		
	}
	
	
	
	
	
	public final static void renderHoverButton(String title, int myX, int myY , GUIListener myListener) {
		if (GameInput.controller1.isMouse && GameInput.cursorX >= myX && GameInput.cursorX <= myX + 30 && GameInput.cursorY >= myY && GameInput.cursorY <= myY + 14) {
			if (buttonHover!=1) {
				buttonHover=1;
//					Audio.playSound(Audio.FX_UISELECT);
			}
		} else {
			if (GameInput.controller1.isMouse && buttonHover==1) buttonHover=-1;
		}
		
		int targetWidth=calculateWidth(title,0);
		if (targetWidth<31) targetWidth=31;

		
		if (buttonHover==1) {
			int tx=myX;
			// left cap of button
			Render.dest.set(myX, myY, myX+ 15, myY+ 10);
			Render.src.set(0, 221, 15, 231);
			Render.drawBitmap(guiImage);
			tx+=15;
			while (tx<myX+targetWidth-16)
			{
				Render.dest.set(tx, myY, tx+10, myY+ 10);
				Render.src.set(8, 221, 18, 231);
				Render.drawBitmap(guiImage);
				tx+=10;
				if (tx>myX+targetWidth-16) tx=myX+targetWidth-16;
			}
			// right cap
			Render.dest.set(tx, myY, tx+ 16, myY+ 10);
			Render.src.set(15, 221, 31, 231);
			Render.drawBitmap(guiImage);
		} else {
			int tx=myX;
			// left cap of button
			Render.dest.set(myX, myY, myX+ 15, myY+ 10);
			Render.src.set(32, 221, 47, 231);
			Render.drawBitmap(guiImage);
			tx+=15;
			while (tx<myX+targetWidth-16)
			{
				Render.dest.set(tx, myY, tx+10, myY+ 10);
				Render.src.set(40, 221, 50, 231);
				Render.drawBitmap(guiImage);
				tx+=10;
				if (tx>myX+targetWidth-16) tx=myX+targetWidth-16;
			}
			// right cap
			Render.dest.set(tx, myY, tx+ 16, myY+ 10);
			Render.src.set(46, 221, 63, 231);
			Render.drawBitmap(guiImage);			
		}
		GUI.renderText(title, 0, myX+8, myY+2, 180, 0, 0);

		// Activate/select inventory item?
		if (buttonHover==1 &&
				((GameInput.controller1.isTouchscreen && GameInput.touchReleased && GameInput.touchX >= myX && GameInput.touchY >= myY - 8 && GameInput.touchX <= myX + 30 && GameInput.touchY <= myY + 20) 
				|| (GameInput.controller1.isMouse && GameInput.cursorX >= myX && GameInput.cursorX <= myX + 30 && GameInput.cursorY >= myY - 8 && GameInput.cursorY <= myY + 20 && GameInput.controller1.BUTTON_X && !GameInput.controller1.BUTTON_Xlocked))
			) 
		{
			if (GameInput.controller1.BUTTON_X) GameInput.controller1.BUTTON_Xlocked=true;
			else if (GameInput.controller1.isTouchscreen) GameInput.touchReleased=false;

//			Audio.playSound(Audio.FX_UISELECT);		
			myListener.onSelected();
		}		
	}
	
}

