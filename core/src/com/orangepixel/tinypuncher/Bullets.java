package com.orangepixel.tinypuncher;

import com.orangepixel.tinypuncher.ai.Monster;
import com.orangepixel.utils.Render;

/**
 * Bullet handling (anything from actual bullets to explosions or triggers)
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class Bullets extends Entity {
	
	public final static int		EXPLOSION = 0;

	
	// various owner states of a bullet
	public final static int		bOWNER_PLAYER = 0,	// correspond to playerid
								bOWNER_ANYONE = 1,
								bOWNER_MONSTER = 2;	
	

	// movement
	public int				targetX;
	public int				targetY;
	public int				xSpeed;
	public int				ySpeed;
	public int				xSpeedInc;
	public int				ySpeedInc;
	

	// looks
	public int				SpriteSet;
	public int				xOffsetAdd;
	public int				xOffsetMax;
	public int				animDelay;
	public int				animSpeed;
	
	
	// states
	public int				myOwner;
	public int				energy;
	public int				lifeForce;
	
	
	// entity information
	public int				myType;
	public int				subType;

	
	static Bullets[] bulletList=new Bullets[128];

	public Bullets() {
		deleted=false;
	}
	
	
	public final static void initBullets() {
		for (int i=bulletList.length - 1; i >= 0; i--) bulletList[i]=new Bullets();
		resetBullets();
	}
	
	public final static void resetBullets() {
		for (int i=0; i<bulletList.length; i++) bulletList[i].deleted=true;
	}
	
	// add a new Bullet to the pool
	public final static int addBullet(int mOwner, int mXPLevel, int mType, int aX, int aY, int mSubType, int mTargetX, int mTargetY, World myWorld) {
		int i=0;
		while (i<bulletList.length && !bulletList[i].deleted) i++;
		if (i<bulletList.length) {
			bulletList[i].init(mOwner, mType, aX,aY, mSubType, mXPLevel,myWorld, mTargetX,mTargetY);
			bulletList[i].UID=Entity.getUID();
			return i;
		}
		return -1;
	}		
	
	
	
	// init this object
	public void init(int mOwner, int mType, int aX,int aY, int mSubType, int mXPLevel, World myWorld, int mTargetX, int mTargetY) {
		
		// generate a unique random int for each monster
		int t=(UID*mType) ^ ((aX<<UID) << 11);
		myRandom=myRandom ^ (myRandom >> 19) ^ (t ^ (t >> 8));
		
		// we are now concidered active monster
		deleted=false;
		died=false;
		visible=true;
		
		subType=mSubType;
		myType=mType;
		myOwner=mOwner;
		x=aX;
		y=aY;
		xSpeed=0;
		ySpeed=0;

		// defaults
		SpriteSet=4;
		alpha=255;
		rotate=0;
		aiState=0;
		lifeForce=32;
		
		energy=mXPLevel;
		
		
		
		switch (myType)
		{
			case EXPLOSION:
				lifeForce=1;
				if (mTargetX==Globals.RIGHT) xSpeed=16;
				else xSpeed=-16;
				
				w=energy;
				h=energy;
				x-=(w>>1);
				y-=(h>>1);
				visible=false;
			break;
		}

		
		floatX=x<<4;
		floatY=y<<4;
	}
	

	
	public final void updatebullet(Player tmpPlayer, World myWorld) {
		if (lifeForce>0) lifeForce--;
		else died=true;
		
		switch (myType)
		{
			case EXPLOSION:
				if (myOwner!=bOWNER_PLAYER && tmpPlayer!=null && tmpPlayer.x+10>=x && tmpPlayer.x<x+w && tmpPlayer.y+10>=y && tmpPlayer.y<y+h)
				{
					tmpPlayer.hit(this,myWorld);
					died=true;
				}
			break;
		}
		
	}
	
	
	
	public final void basicMovement() {
		floatX+=xSpeed;
		x=floatX>>4;

		floatY+=ySpeed;
		y=floatY>>4;
	}
	
	
	public final boolean collidesWith(Monster myMonster) {
		boolean collide=false;

		if (myOwner!=bOWNER_ANYONE && myOwner==bOWNER_MONSTER) return false;
		int cw=x+w;
		int ch=y+h;
		
		if ((myMonster.x<=cw) && (myMonster.x+myMonster.w>=x) && (myMonster.y<=ch) && (myMonster.y+myMonster.h>=y)) collide=true;

		return collide;
	}		

	
	
	// update monsters and add live ones to the sprite list
	public final static void update(World myWorld, Player myPlayer) {
		int i=0;
		int tx;
		int ty;
		
		Bullets tmpBullet;
		Monster tmpMonster;
		
		while (i<bulletList.length) 
		{
			tmpBullet=bulletList[i];
			
			if (!tmpBullet.deleted && !tmpBullet.died) 
			{
				
				tmpBullet.updatebullet(myPlayer,myWorld);
				
				
				// check collision with all live monsters
				for (int m=0; m<Monster.monsterList.length; m++) 
				{
					tmpMonster=Monster.monsterList[m];
				
					if (!tmpBullet.died && !tmpMonster.deleted && !tmpMonster.died && tmpBullet.collidesWith(tmpMonster)) 
					{
						if (tmpBullet.myOwner==Bullets.bOWNER_PLAYER) 
						{
							if (tmpMonster.hit(tmpBullet,myWorld)) 
							{
								tmpBullet.died=true;
							}
						} 

					}
				}					
				
				
				
				
				
				if (!tmpBullet.died) 
				{	
					if (tmpBullet.visible)
					{
						tx=tmpBullet.x-World.offsetX;
						ty=tmpBullet.y-World.offsetY;
	
						Render.dest.set(tx,ty,tx+tmpBullet.w, ty+tmpBullet.h);
						Render.src.set(tmpBullet.xOffset, tmpBullet.yOffset, tmpBullet.xOffset+tmpBullet.w, tmpBullet.yOffset+tmpBullet.h);
						Spriter.addSprite(Render.dest, Render.src, tmpBullet.SpriteSet, tmpBullet.alpha, tmpBullet.rotate, 0, false);
					}
				}
				else 
				{
					tmpBullet.deleted=true;
				}
			}
			i++;
		}
	}
	
	
}
