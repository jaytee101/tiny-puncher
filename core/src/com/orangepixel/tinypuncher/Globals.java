package com.orangepixel.tinypuncher;

import java.util.Random;

import com.badlogic.gdx.Gdx;

/**
 * Globals..
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class Globals {
	
	
	public final static boolean DEBUGGING = true;	// set to fale to get rid of debug messages
	
	

	// DIRECTIONS
	public final static int	UP = 0,
							RIGHT = 1,
							DOWN = 2,
							LEFT = 3;
	
	
	// Characters
	public final static int	CHAR_ELF = 0,
							CHAR_WARRIOR = 1,
							CHAR_WIZARD = 2,
							CHAR_VALKYRIE = 3;

	public final static String[] characterName = new String[] {
			"ELF",
			"WARRIOR",
			"WIZARD",
			"VALKYRIE"
	};
	

	
	// DEBRIS TYPES
	public final static int DEBRIS_SMALLWOOD = 0,
							DEBRIS_BLOOD = 1,
							DEBRIS_BONES = 2,
							DEBRIS_ROCKS = 3;
	
	

	
	// section names ------------------------------------------------------------------
	// these correspond to the myWorld.SpriteSet (tile-set)
	public final static String[] levelName = new String[] {
		"castle halleth of entrance",
		"castle librariatha",
		"undergroundeth passageth",
		"old foundationeth",
		"halleth of ancienteth",
		"",
		"",
		"outereth courtyardeth",
		"",
		"",
		"",
		"",
		"",
		"",
		""
	};


	// Text values for special events ---------------------------------------------
	public final static String[] specialInfoEvents = new String[] {
		"heroes of loot 2 - alpha build",
		"a secret area",	// 1
		"out of arrows!",	// 2
		"running out of arrows!", // 3
		"a locked gate",	// 4
		"you found @",			// 5
		"a hidden room nearby",	// 6
		"i sense an item of importance", //7
		"arrows..",			// 8
		"must go west",		// 9
		"must go east",		// 10
		"must go north",		// 11
		"must go south",		// 12
		"elf",				// 13
		"warrior",			// 14
		"wizard",			// 15
		"valkrie",			// 16
	"-----",// 17
		"a big statue", // 18
		"a switch triggered", // 19
		"you died...",	// 20
		"this is not moving..", // 21
		"too heavy for me!", // 22
		"switch needs something heavy", // 23
		"battle: monster spawner", // 24
		"battle: rocko",	// 25
	"---", // 26
		"nothing happens", // 27
		"this @ costs $",	// 28
		"who has a key?",	// 29
		"level up!",		// 30
		"need more coins",	// 31
		"replace old 1 with new 2",	// 32
		"inventory full", // 33
		// shop keeper banter
		"welcome to my shop!", // 34
		"bad for business I tell ya", // 35
		"stinking demon creatures..", // 36
		"stop googling, buy some!", // 37
		"want me to roll the dice?", // 38
		"my motto: items for loot!", // 39
		
		// princess banter
		"oh dear.. this is a crowd!", // 40
		"hihihihi.. hi stranger", // 41

		// princess guards
		"get out of my room!", // 42
		
	};
	
	public final static int startShopKeeperBanter= 34,
							startPrincessBanter = 40,
							startPrincessGuards = 42;
							
	
	// Animation text ---------------------------------------------
	public final static String[] animationText = new String[] {
		"perfect!",
		"guys I found us a new job",
		"are you sure about this?",
		"just trust me",
		"my instinct is never wrong",
		"besides",
		"how bad can it be?",
		"...",
		"here we go again!"
	};
	

	
	public final static int QUEST_FINDKEY = 0,
							QUEST_ROCKO = 1,
							QUEST_ALTAR = 2,
							QUEST_PRESSUREPLATE = 3,
							QUEST_SPAWNER = 4,
							QUEST_SPIKEPIT = 5,
							QUEST_FLAMER = 6,
							QUEST_MAGICCANDLES = 7,
							QUEST_GLYPHS = 8,
							QUEST_MAX	= 9;
	
	// when giving quest
	public final static String[] avatarText = new String[] {
		"the door is locked.. there must be a key somewhere",
		"the only way to open this gate is slay the rocko who grabbed the key..",
		"this door is protected by an altar, it needs a sacrifice",
		"<pressureplate>",
		"there is a monster-generator nearby, we can't open this gate until it's destroyed",
		"<spikepit>",
		"we must first put out the fire before opening this gate!",
		"this gate is under a magical protection",
		"magical runes are keeping this gate down"
	};
	
	

	
	
	
	
	public final static void debug(String message) {
		if (DEBUGGING) Gdx.app.log("opdebug",message);
	}

	
	
	/**
	 * Random seeding table generation
	 * 
	 * Supply three seed values to start generating random seed table
	 * the table will always be the same if the same values are specified
	 * 
	 * so specifying day,month,year will get you a randomtable with a daily seed 
	 *  
	 */
	// We need random values! and this makes it easier
	public static Random randomGenerator = new Random();
	// 4th seed value, pre defined
	static int randw=1;

	public static int randomNextInt;
	private static int[] randomTable=new int[60000];
	
	// set this true to force random value from the seed table
	public static boolean getRandomSeeded=true;
	
	/* ==================
	 * specify 3 different seeds values to generate the random table
	 * ==================
	 */
	public static void fillRandomTable(int randx, int randy, int randz) {
		int t;

		int s1=randx;
		int s2=randy;
		int s3=randz;
		
		for (int i=0; i < 60000; i++) {
			t=s1 ^ (s1 << 11);
			s1=s2;
			s2=s3;
			s3=randw;
			randw=randw ^ (randw >> 19) ^ (t ^ (t >> 8));
			randomTable[i]=randw;
		}

		randomNextInt=0;
	}
	
	/* ==================
	 * always returns the next random-table seeded value between 0 and provided value
	 * ==================
	 */
	public static int getRandomSeeded(int digit1) {
		int value=randomTable[randomNextInt] % digit1;
		randomNextInt++;
		if (randomNextInt == 60000) randomNextInt=0;
		return value;
	}
	
	/* ==================
	 * if getRandomSeeded is true, it returns a value from the seed table
	 * else it returns from standard random generator (time based)
	 * between 0 and provided value
	 * ==================
	 */
	public static int getRandom(int digit1) {
		if (getRandomSeeded) return getRandomSeeded(digit1);
		else  return  randomGenerator.nextInt(digit1);
	}	

	/* ==================
	 * always returns a non-seeded random value (time based)
	 * between 0 and provided value
	 * ==================
	 */
	public static int getRandomForcedUnseeded(int digit1) {
		return  randomGenerator.nextInt(digit1);
	}	
	

}
