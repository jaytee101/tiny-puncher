package com.orangepixel.tinypuncher.ai;

import com.orangepixel.tinypuncher.Audio;
import com.orangepixel.tinypuncher.Bullets;
import com.orangepixel.tinypuncher.FX;
import com.orangepixel.tinypuncher.Globals;
import com.orangepixel.tinypuncher.Player;
import com.orangepixel.tinypuncher.World;
import com.orangepixel.utils.Render;

/**
 * foot soldiers
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class Brawlers {

	// brawler characters
	public final static int		BALD = 0,
								BLACK = 1;
			
	

	
	public final static int propFrameX = 0,
							propFrameY = 1,
							propW = 2,
							propH = 3;

	private final static int[][] properties = new int[][]
	{
		{0,74,10,10},
		{0,85,10,9}
	};
	
	
	
	public final static int		animIdle = 0,
								animWalking = 1,
								animBrawl = 2,
								animHurlThruAir = 3,
								animFlatOnFloor = 4;
	
	public final static int[][] animationFrames = new int[][] {
		{ 0,1,2 },
		{ 4,5 },
		{ 6,7,6,7,6,7,6,7,8,7,6,7,9,8,6,9 },
		{ 10 },
		{ 11 }
};
	
	
	
	
	public final static int 	aiPreActive = 0,
								aiIdle=1,
								aiBrawl = 2,
								aiHurl = 3,
								aiOnFloor = 4,
								aiDied = 5;
	
	
	/* 
	 * ================
	 * initialise provided monster into our type
	 * ================
	 */
	
	public final static void init(Monster e, World myWorld) 
	{
		e.isDangerous=false;
		e.startX=e.x;
		e.startY=e.y;
		e.aiState=aiIdle;
		e.dangerLevel=0;
		e.fireDelay=0;

		e.animationIndex=Globals.getRandom(3);
		
		e.xOffset=properties[e.subType][propFrameX];
		e.yOffset=properties[e.subType][propFrameY];
		e.w=properties[e.subType][propW];
		e.h=properties[e.subType][propH];
		e.z=0;
		e.depth=0;
		
		e.maxStamina=1;
		e.stamina=e.maxStamina;
		e.life=2;

		if (World.inGame)
		{
			e.aiState=aiBrawl;
			World.enemyCount++;
		}
		else
		{
			e.aiState=aiPreActive;
		}
		e.aiCountdown=0;
		
	}
	
	
	/*
	 * ================
	 *  called after map generation is done, so some objects can turn 
	 *  them selves into "solid" wall type objects before the player starts
	 * ================
	 */
	public final static void solidify(Monster e,World myWorld) 
	{
		
		switch (e.subType)
		{
		}
		
	}
	
	
	
	/*
	 * ================
	 * update specified monster every tick
	 * ================
	 * 	
	 */
	public final static void update(Monster e, World myWorld, Player tmpPlayer) 
	{

		if (e.fireDelay>0) e.fireDelay--;
		
				
				switch (e.aiState)
				{
					case aiPreActive:
						e.visible=false;
						if (World.enemyCount>=8 || myWorld.CameraTakeOver) return;
						
						if (tmpPlayer.x>=e.startX-8 && tmpPlayer.x<e.startX+8)
						{
							if (e.myRandom(100)>50 && World.offsetX>64)
							{
								e.x=World.offsetX-e.w;
								e.floatX=e.x<<4;
								e.aiState=aiBrawl;
								e.visible=true;
								e.activated=true;
								
								World.enemyCount++;
							}
							else
							{
								e.x=World.offsetX+Render.width;
								e.floatX=e.x<<4;
								e.aiState=aiBrawl;
								e.visible=true;
								e.activated=true;
								
								World.enemyCount++;
							}
						}
						
						return;

				
				
					case aiIdle:
						e.animationType=animIdle;
						
						
						if (e.aiCountdown>0)
						{
							e.aiCountdown--;
						}
						else if (e.activated || (tmpPlayer.x>e.x-48 && tmpPlayer.x<e.x+64 && tmpPlayer.y>e.y-48 && tmpPlayer.y<e.y+58))
						{
							e.aiState=aiBrawl;
							e.activated=true;
						}
					break;
					
					
					
					
					
					case aiBrawl:
						e.visible=true;
						e.animationType=animIdle;
						
						
						

						if (e.aiCountdown>0)
						{
							e.aiCountdown--;
						}
						else
						{
							e.aiCountdown=8+e.myRandom(14);
							
							e.targetX=(tmpPlayer.x-10)+e.myRandom(20);
							e.targetY=(tmpPlayer.y-10)+e.myRandom(20);
						}

						
						// walk towards target spot
						if (e.targetX<e.x)
						{
							e.myDirection=Globals.LEFT;
							e.xSpeed=-8;
							e.animationType=animWalking;
							
						}
						else if (e.targetX>e.x)
						{
							e.myDirection=Globals.RIGHT;
							e.xSpeed=8;
							e.animationType=animWalking;
							
						}
						else
						{
							e.xSpeed=0;
						}
						
						// do movement and collision detection
						e.doHorizontal(myWorld);
						
						
						e.doGravity(myWorld);
						
						if (e.zSpeed>-64) e.zSpeed-=4;
//						if (e.zSpeed<-4) e.animationType=animFallDown;
						

						// vertical positioning
						if (e.targetY<e.y)
						{
							e.ySpeed=-8;
							e.animationType=animWalking;
						}
						else if (e.targetY>e.y)
						{
							e.ySpeed=8;
							e.animationType=animWalking;
						}
						else
						{
							e.ySpeed=0;
						}
						
						e.doVertical(myWorld);
						
						
//						if (myWorld.isSolid((e.x+(e.w>>1))>>3, (e.y+e.h-1)>>3,4))
//						{
//							e.z=myWorld.getTile((e.x+(e.w>>1))>>3, (e.y+e.h-1)>>3);
//							World.enemyCount--;
//							return;
//						}
						
						
						/*
						if (tmpPlayer.x+5>e.x+5)
						{
							e.yOffset=properties[e.subType][propFrameY];
						}
						else
						{
							e.yOffset=properties[e.subType][propFrameY]+properties[e.subType][propH];
						}
						*/

						
						
						// close enough to land a punch?
						if (tmpPlayer.y+10>=e.y+e.h-2 && tmpPlayer.y+10<e.y+e.h+1 && tmpPlayer.x+9>=e.x && tmpPlayer.x+1<e.x+e.w) 
						{
							e.targetY=(tmpPlayer.y-10)+e.myRandom(20);

							e.animationType=animBrawl;
							// delay re-tracking position
							e.aiCountdown=8;
							
							if (e.fireDelay==0)
							{
								Bullets.addBullet(Bullets.bOWNER_MONSTER, e.myRandom(3), Bullets.EXPLOSION, e.x+5, e.y+5, 14, e.myDirection,0, myWorld);
								e.fireDelay=32;
							}
							
						}
						
					break;
					
					
					
					
					
					case aiHurl:
						e.visible=true;
						e.animationType=animHurlThruAir;
						e.ySpeed=0;
						
						if (e.zSpeed>-48) 
						{
							e.zSpeed-=4;
						}
						e.floatZ+=e.zSpeed;
						
						if (e.floatZ<=0)
						{
							e.zSpeed=0;
							e.floatZ=0;

							tmpPlayer.score+=10;
							
							FX.addFX(FX.fDIGITPLUME, e.x, e.y-2, 10, myWorld);

							World.worldShake=3;
							e.aiState=aiOnFloor;
							e.aiCountdown=64;
							FX.addFX(FX.fPUFF, e.x, e.y+e.h-4, FX.puffSmallSmoke, myWorld);
							Audio.playSoundPitched(Audio.FX_LAND1);
						}
						e.z=e.floatZ>>4;
						
						
						e.doHorizontal(myWorld);
						
						e.doVertical(myWorld);
						
					break;
					
					
					
					
					case aiOnFloor:
						e.animationType=animFlatOnFloor;
						
						if (e.aiCountdown>0)
						{
							e.aiCountdown--;
						}
						else
						{
							if (e.life<=0)
							{
								e.aiState=aiDied;
								e.aiCountdown=16;
								
								World.enemyCount--;
								
								Monster.addMonster(Monster.mPICKUP, e.x, e.y, Pickup.COIN, myWorld);
							}
							else
							{
								e.aiState=aiIdle;
								e.aiCountdown=16;
								e.stamina=e.maxStamina;
							}
						}
						
						e.xSpeed=0;
					break;
					
					case aiDied:
						if (e.aiCountdown>0) 
						{
							e.aiCountdown--;
						}
						else 
						{
							e.died=true;
							tmpPlayer.score+=100;

							FX.addFX(FX.fDIGITPLUME, e.x, e.y-2, 100, myWorld);
						}
						
						e.visible=(e.aiCountdown%4<2);
					break;
				}

				
				doAnimation(e);						
				
				FX.addFX(FX.fFLOORMARK, e.x, e.y+7, FX.floorShade, myWorld);

	}
	
	
	
	/* ==============
	 * animate player frames based on current state
	 * ==============
	 */
	private final static void doAnimation(Monster e) {

		if (e.animDelay>0)
		{
			e.animDelay--;
		}
		else
		{
			e.animDelay=8;
			e.animationIndex++;
		}
			
		if (e.animationIndex>animationFrames[e.animationType].length-1) e.animationIndex=0;
		
		e.xOffset=properties[e.subType][propFrameX]+(animationFrames[e.animationType][e.animationIndex]*e.w);
		
	}
	
	
	/* ==============
	 * the collision is checked, we might just wanna ignore it on type of bullet, or special state
	 * ==============
	 */
	public final static boolean hit(Monster e,Bullets myBullet, World myWorld)
	{

		if (e.aiState==aiHurl || e.life<=0) return false;

		FX.addFX(FX.fDEBRI, myBullet.x, myBullet.y, FX.debriBlood, myWorld);
		
		Audio.playRandomThump();
		
		e.stamina--;
		if (e.stamina<0)
		{
			
			Audio.playRandomMaleOuch();
			
			e.life--;
			
			e.activated=true;
			
			e.aiState=aiHurl;
			e.zSpeed=(32+e.myRandom(8));
			
			if (myBullet.xSpeed<0) 
			{
				e.xSpeed=-(16+e.myRandom(8));
				e.myDirection=Globals.LEFT;
//				e.yOffset=properties[e.subType][propFrameY]+properties[e.subType][propH];
			}
			else
			{
				e.xSpeed=(16+e.myRandom(8));
				e.myDirection=Globals.RIGHT;
//				e.yOffset=properties[e.subType][propFrameY];
			}
		}
		
		return true;
	}
	
}
