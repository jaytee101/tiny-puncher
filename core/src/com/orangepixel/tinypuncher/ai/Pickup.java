package com.orangepixel.tinypuncher.ai;

import com.orangepixel.tinypuncher.Audio;
import com.orangepixel.tinypuncher.FX;
import com.orangepixel.tinypuncher.Globals;
import com.orangepixel.tinypuncher.Player;
import com.orangepixel.tinypuncher.World;


/**
 * pickup items (coins, powerups, etc)
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class Pickup {

	// object types
	public final static int COIN = 0;
	
	public final static int propFrameX = 0,
			propFrameY = 1,
			propW = 2,
			propH = 3;

	private final static int[][] properties = new int[][]
	{
		{87,0,8,8},		// coin
	};	

	
	public final static int 	aiIdle=0,
								aiCoinFly=1,
								aiCoin=2;
	
	
	/* 
	 * ================
	 * initialise provided monster into our type
	 * ================
	 */
	
	public final static void init(Monster e) 
	{
		e.isDangerous=false;
		e.startX=e.x>>3;
		e.startY=e.y>>3;
		e.aiState=aiIdle;
		e.dangerLevel=0;
		e.fireDelay=0;

		
		e.xOffset=properties[e.subType][propFrameX];
		e.yOffset=properties[e.subType][propFrameY];
		e.w=properties[e.subType][propW];
		e.h=properties[e.subType][propH];
		
		switch (e.subType)
		{
			case COIN:
				e.depth=0;
				e.animDelay=0;
				e.aiState=aiCoinFly;
				
				switch (Globals.getRandomForcedUnseeded(40)%10) {
					default:
						e.xSpeed=-2+Globals.getRandomForcedUnseeded(4);
						e.ySpeed=-(6+Globals.getRandomForcedUnseeded(4));
						e.targetY=(e.y-10)+Globals.getRandomForcedUnseeded(16);
					break;
					
					case 1:
						e.xSpeed=2+Globals.getRandomForcedUnseeded(4); 
						e.ySpeed=-(4+Globals.getRandomForcedUnseeded(4));
						e.targetY=(e.y-10)+Globals.getRandomForcedUnseeded(16);
					break;
					
					case 2:
						e.xSpeed=-1+Globals.getRandomForcedUnseeded(4);
						e.ySpeed=-(3+Globals.getRandomForcedUnseeded(4));
						e.targetY=(e.y-8)+Globals.getRandomForcedUnseeded(16);
					break;
					
					case 3:
						e.xSpeed=1+Globals.getRandomForcedUnseeded(4); 
						e.ySpeed=-(3+Globals.getRandomForcedUnseeded(4));
						e.targetY=(e.y-2)+Globals.getRandomForcedUnseeded(12);
					break;
				}				
			
				e.xSpeed=e.xSpeed<<3;
				e.ySpeed=e.ySpeed<<3;				
			break;
	
			
		}
	
		e.activated=true;
	}
	
	
	/*
	 * ================
	 *  called after map generation is done, so some objects can turn 
	 *  them selves into "solid" wall type objects before the player starts
	 * ================
	 */
	public final static void solidify(Monster e,World myWorld) 
	{
		
		switch (e.subType)
		{
			

		}
		
	}
	
	
	
	/*
	 * ================
	 * update specified monster every tick
	 * ================
	 * 	
	 */
	public final static void update(Monster e, World myWorld, Player tmpPlayer) 
	{
		switch (e.aiState)
		{
			case aiCoinFly:
				if (e.animDelay>0) 
				{
					e.animDelay--;
				}
				else
				{
					e.animDelay=8;
					e.xOffset+=e.w;
					if (e.xOffset>109)
					{
						e.xOffset=87;
					}
				}
				
				
				e.floatX+=e.xSpeed;
				e.floatY+=e.ySpeed;
				e.x=e.floatX>>4;
				e.y=e.floatY>>4;
				
				if (e.ySpeed<13<<4) e.ySpeed+=8;
				
				if (e.y>=e.targetY) {
					e.aiState=aiCoin;
					e.aiCountdown=200;
					if (myWorld.isSolid(e.x>>3, e.y>>3,2)) e.died=true;
				}				
			break;
			
			
			
			case aiCoin:
				if (e.animDelay>0) 
				{
					e.animDelay--;
				}
				else
				{
					e.animDelay=8;
					e.xOffset+=e.w;
					if (e.xOffset>109)
					{
						e.xOffset=87;
					}
				}	
				
				if (e.aiCountdown>0)
				{
					e.aiCountdown--;
					if (e.aiCountdown==0) e.died=true;
				}
				
				if (e.aiCountdown%4<2 && e.aiCountdown<64) e.visible=!e.visible;
				
				if (e.hitPlayer(tmpPlayer))
				{
					e.died=true;
					FX.addFX(FX.fDIGITPLUME, e.x, e.y, 5, myWorld);
					Audio.playSound(Audio.FX_COIN);
				}
				
				FX.addFX(FX.fFLOORMARK, e.x, e.y+7, FX.floorShade, myWorld);
			break;
		}
		
		
		
	}

	
}
