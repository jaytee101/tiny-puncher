package com.orangepixel.tinypuncher.ai;

import com.orangepixel.tinypuncher.Audio;
import com.orangepixel.tinypuncher.Bullets;
import com.orangepixel.tinypuncher.Entity;
import com.orangepixel.tinypuncher.Globals;
import com.orangepixel.tinypuncher.Player;
import com.orangepixel.tinypuncher.Spriter;
import com.orangepixel.tinypuncher.World;
import com.orangepixel.utils.Render;


/**
 * Base Monster class, entry point for all monster code handling
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class Monster extends Entity {
	
	
	public final static int		mSCENERY = 0,
								mBRAWLER = 1,
								mPICKUP = 2;
	
	
	
	static public Monster[] monsterList=new Monster[1024];
	
	static public int DefaultSpriteSet = 4;
	
	
	public int		targetX;
	public int		targetY;
	public int		startX;
	public int		startY;
	public boolean	onGround;
	int				depth;
	int				originX;
	int				originY;
	int				xSpeed;
	int				ySpeed;
	int				zSpeed;
	int				ySpeedIncrease;
	int				myDirection;

	
	// looks
	public int		SpriteSet;
	int				xOffsetAdd;
	int				animDelay;
	int				animationIndex;
	int				animationType;
	public int		hitCounter;
	int				hitDelay;

	
	boolean			canBeSmashed;
	public boolean	isDangerous;
	boolean			isPushable;
	
	
	
	// entity information
	public int				strength;
	public int				dangerLevel;	// used for player to determine which is more dangerous with auto-aim
	public int				myQuestID;
	public int				myRoomID;
	
	int						myPlayerid;
	public int				myType;
	public int				subType;
	
	
	
	
	/* ==============
	 * initialise the object pool with monster objects, call once at engine init
	 * ==============
	 */
	public final static void initMonster(int mDefaultSpriteSet) {
		for (int i=monsterList.length - 1; i >= 0; i--) monsterList[i]=new Monster();
		resetMonsters();
	}
	
	/* ==============
	 * set all monsters in the object pool to a deleted state
	 * ==============
	 */
	public final static void resetMonsters() {
		for (int i=0; i<monsterList.length; i++) monsterList[i].deleted=true;
	}


	/* ==============
	 * Add a new monster to the pool
	 * It will re-use any "deleted" monster slots in the array
	 * ==============
	 */
	public final static int addMonster(int mType, int aX, int aY, int mSubType, World myWorld) {
		int i=0;
		
		while (i<monsterList.length && !monsterList[i].deleted) i++;
		
		if (i<monsterList.length) 
		{
			monsterList[i].init(mType, aX, aY, mSubType, myWorld);
			monsterList[i].UID=Entity.getUID();
			
			return i;
		}
		return -1;
	}	
	
	
	
	public final static Monster getMonster(int idx) {
		return monsterList[idx];
	}
	
	
	
	
	/*==============
	 * (re)initialise this monster object
	 * ==============
	 */
	public void init(int mType, int aX,int aY, int mSubType, World myWorld) 
	{
		// we are now concidered active monster
		deleted=false;
		died=false;
		dangerLevel=0;

		myPlayerid=-1;
		
		myType=mType;
		subType=mSubType;
		SpriteSet=DefaultSpriteSet;
		
		// generate a unique random int for each monster
		myRandom=Globals.getRandom(2048); //<<UID); // (myType*subType) ^ (UID >> 19) ^ (t ^ (t >> 8));

		myRoomID=-1;
		
		// defaults
		activated=false;
		alpha=255;
		rotate=0;
		aiState=0;
		aiCountdown=0;
		xSpeed=0;
		ySpeed=0;
		movementDelay=0;
		visible=true;
		isPushable=false;
		onGround=false;
		myDirection=Globals.RIGHT;
		
		depth=0;
		
		hitCounter=0;
		hitDelay=0;
		myQuestID=-1;
		
		// If aX+aY are negative, we translate to (positive) tile-locations instead of screen coordinates
		if (aX<0 && aY<0) 
		{
			x=-(aX<<3);
			y=-(aY<<3);
			startX=-aX;
			startY=-aY;
		} 
		else 
		{
			x=aX;
			y=aY;
			startX=aX>>3;
			startY=aY>>3;
		}
		
		isDangerous=true;
		activatedDelay=16;

		switch (mType) 
		{
			case mSCENERY:
				Scenery.init(this);
			break;
			
			case mBRAWLER:
				Brawlers.init(this,myWorld);
			break;
			
			case mPICKUP:
				Pickup.init(this);
			break;
		}
		
		floatX=x<<4;
		floatY=y<<4;
		
	}	
	
	
	/*==============
	 * Some objects need to make their tile-map location solid before game starts
	 * ==============
	 */
	public final void solidify(World myWorld) 
	{
		if (died || deleted) return;
		
		switch (myType) 
		{
			case mSCENERY:
				Scenery.solidify(this, myWorld);
			break;
			
			case mBRAWLER:
				Brawlers.solidify(this, myWorld);
			break;
			
			case mPICKUP:
				Pickup.solidify(this, myWorld);
			break;
		}
	}
	
	
	
	public final boolean hitPlayerFeet(Player tmpPlayer) {
		if (tmpPlayer.x<x+w && tmpPlayer.x+tmpPlayer.w>x && tmpPlayer.y+tmpPlayer.h-2<y+h && tmpPlayer.y+tmpPlayer.h>y+h-4) return true;
		return false;
	}

	public final boolean hitPlayer(Player tmpPlayer) {
		if (tmpPlayer.x<x+w && tmpPlayer.x+tmpPlayer.w>x && tmpPlayer.y<y+h && tmpPlayer.y+tmpPlayer.h>y+h-1) return true;
		return false;
	}


	
	/*==============
	 * object update, call every tick
	 * ==============
	 */
	public final void updateMonster(Player myPlayer, World myWorld) {
		
		if (myWorld.CameraTakeOver && isDangerous) return;

		
		if (hitCounter>0) hitCounter--;

		// some monsters will "freeze" for a second when hit
		if (hitDelay>0 && aiState<900)
		{
			hitDelay--;
			return;
		}
		
		
		
		x=floatX>>4;
		y=floatY>>4;
		
		
		
		
		switch (myType) 
		{
			case mSCENERY:
				Scenery.update(this, myWorld, myPlayer);
			break;
			
			case mBRAWLER:
				Brawlers.update(this, myWorld, myPlayer);
			break;
			
			case mPICKUP:
				Pickup.update(this, myWorld, myPlayer);
			break;	
		}
		
		
		
	}
	
	
	
	
	// do vertical movement + collision check with the world
	public final void doVertical(World myWorld) {
		int tx;
		int tx2;
		int ty;
		
		floatY+=ySpeed;
		y=floatY>>4;
		tx=(x+w-2)>>3;
		tx2=(x+2)>>3;
		
		if (ySpeed<0) 
		{
			ty=(y+h-2)>>3;
			if (myWorld.isSolid(tx,ty,3+z) || myWorld.isSolid(tx2,ty,3+z)) 
			{
				y=(ty<<3)+8-(h-2);
				floatY=y<<4;
				ySpeed=0;
			}
		} 
		else if (ySpeed>=0) 
		{
			ty=(y+h-1)>>3;
			if (myWorld.isSolid(tx,ty,3+z) || myWorld.isSolid(tx2,ty,3+z)) 
			{
				y=((ty<<3)-(h-1));
				floatY=y<<4;
				ySpeed=0;
			}
		}
		
	}
	

	
	// do horizontal movement + collision check with the world
	public final void doHorizontal(World myWorld) {
		int tx;
		int ty;
		
		floatX+=xSpeed;
		x=floatX>>4;
		
		if (xSpeed<0) 
		{
			// collision check left
			tx=(x>>3);
			ty=(y+h-2)>>3;
		
			if (myWorld.isSolid(tx,ty,2+z)) 
			{
				xSpeed=0;
				x=(tx<<3)+8;
				floatX=x<<4;
			}
			
			
		} 
		else 
		{
			// collision check right
			tx=(x+w)>>3;
			ty=(y+h-2)>>3;
		
			if (myWorld.isSolid(tx,ty,2+z)) 
			{
				xSpeed=0;
				x=(tx<<3)-(w+1);
				floatX=x<<4;
			}			
		}
		
	}
	
	
	
	/* =============
	 * handle jumping (z-axis)
	 * =============
	 */
	public final void doGravity(World myWorld) {
		floatZ+=zSpeed;
		z=floatZ>>4;
				
		if (z<=0)
		{
			z=0;
			floatZ=0;
			zSpeed=0;
			onGround=true;
		}

		if (zSpeed<=0) 
		{
			int ty=(y+h-2)>>3;
			int tx=(x+w-2)>>3;
			int tx2=(x+2)>>3;
		
			if (myWorld.isSolid(tx,ty,z)) 
			{
				if (!onGround) Audio.playSoundPitched(Audio.FX_LAND1);

				onGround=true;
				z=myWorld.getTile(tx, ty);
				floatZ=z<<4;
				zSpeed=0;
			}
			else if (myWorld.isSolid(tx2,ty,z))
			{
				if (!onGround) Audio.playSoundPitched(Audio.FX_LAND1);

				onGround=true;
				z=myWorld.getTile(tx2, ty);
				floatZ=z<<4;
				zSpeed=0;
			}
			else
			{
				onGround=false;
			}
		}		
	}
	
	
	/*
	 * ==================
	 * check if energy died, 
	 * if so we go into ready_for_death state (999)
	 * add score, and throw some blood+bones
	 * 
	 * if not, we show a hit-plume of energy we lost
	 * ==================
	 */
/*	
	public void defaultDieTest(Bullets myBullet, World myWorld) {
		if (energy<=0)
		{
			myPlayerid=myBullet.myOwner;
			
			throwBloodAndBones(myWorld);
			
			World.monsterSlayed++;
			myWorld.addScore(5,x,y);
			aiState=999;
			
			
			if (myBullet.myOwner<4)
			{
				for (int xp=dangerLevel>>1; --xp>=0;) 
				{
					Player.playerList[myBullet.myOwner].addXP(x,y,myWorld);
				}
			}
		}
		else
		{
			FX.addFX(FX.fPLUME, x, y-4, -myBullet.energy,myWorld);
		}
	}
*/	
	
	
	// do we respond to being hit by this bullet?
	public boolean hit(Bullets myBullet, World myWorld) {
		
		switch (myType) 
		{
			
			case mBRAWLER:
				return Brawlers.hit(this,myBullet, myWorld);

		}
		
		
		return false;

	}


	

	// update monsters and add live ones to the sprite list
	public final static void update(World myWorld, Player myPlayer) {
		int i=0;
		int tx;
		int ty;
		
		Monster tmpMonster;
		
		while (i<monsterList.length) 
		{
			tmpMonster=monsterList[i];
			
			if (!tmpMonster.deleted && !tmpMonster.died) 
			{
				
				tmpMonster.updateMonster(myPlayer,myWorld);
				
				if (!tmpMonster.died)  
				{
					if (tmpMonster.visible)
					{
						tx=tmpMonster.x-World.offsetX;
						ty=(tmpMonster.y-tmpMonster.z)-World.offsetY;
	
						Render.dest.set(tx,ty,tx+tmpMonster.w, ty+tmpMonster.h);
						Render.src.set(tmpMonster.xOffset, tmpMonster.yOffset, tmpMonster.xOffset+tmpMonster.w, tmpMonster.yOffset+tmpMonster.h);
						Spriter.addSprite(Render.dest, Render.src, tmpMonster.SpriteSet, tmpMonster.alpha, tmpMonster.rotate, tmpMonster.depth, (tmpMonster.myDirection==Globals.LEFT));
					}
				} 
				else 
				{
					tmpMonster.deleted=true;
				}
			}
			i++;
		}
	}
	
	

}
