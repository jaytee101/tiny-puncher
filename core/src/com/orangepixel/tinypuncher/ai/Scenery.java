package com.orangepixel.tinypuncher.ai;

import com.orangepixel.tinypuncher.FX;
import com.orangepixel.tinypuncher.Globals;
import com.orangepixel.tinypuncher.Player;
import com.orangepixel.tinypuncher.World;


/**
 * Scenery (tree's, cars, etc)
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */

public class Scenery {

	
	
	// SCENERY object types
	public final static int TREE = 0,
							TREETOP = 1,
							FIREHYDRANT = 2,
							CAR1 = 3,
							CAR2 = 4,
							CAR3 = 5,
							GARAGE = 6,
							SUBWAYPILAR = 7;
							

	
	public final static int propFrameX = 0,
							propFrameY = 1,
							propW = 2,
							propH = 3;

	private final static int[][] properties = new int[][]
	{
		{0,16,31,45},		// tree
		{0,16,31,31},		// tree-top
		{32,16,6,9},		// fire hydrant
		{39,13,34,24},		// car1
		{74,13,34,24},		// car2
		{109,13,34,24},		// car3
		{150,0,52,31},		// garage
		{203,0,16,72},		// pilar
	};
	
	
	
	
	public final static int 	aiIdle=0,
								aiCAR = 1,
								aiGarageClosed = 2,
								aiGarageOpen = 3,
								aiTreetop = 4;
	
	
	/* 
	 * ================
	 * initialise provided monster into our type
	 * ================
	 */
	
	public final static void init(Monster e) 
	{
		e.isDangerous=false;
		e.startX=e.x>>3;
		e.startY=e.y>>3;
		e.aiState=aiIdle;
		e.dangerLevel=0;
		e.fireDelay=0;

		
		e.xOffset=properties[e.subType][propFrameX];
		e.yOffset=properties[e.subType][propFrameY];
		e.w=properties[e.subType][propW];
		e.h=properties[e.subType][propH];

		e.depth=0;

		switch (e.subType)
		{
			case TREE:
				// place it nicely tile-wise (for collision detection stuff)
				e.x=((e.x>>3)<<3)-4;
			break;
			
			case TREETOP:
				e.x=((e.x>>3)<<3)-4;
				e.depth=128;
				e.aiState=aiTreetop;
			break;
			
			case CAR1:
				e.x--;
				e.y=((e.y>>3)<<3);
				e.depth=0;
				if (e.y>=100) 
				{
					e.yOffset+=e.h+1;
				}
				e.targetY=e.y;
				e.aiState=aiCAR;
			break;
			
			case CAR2:
				e.x--;
				e.y=((e.y>>3)<<3);
				
				e.depth=0;
				if (e.y>=100) 
				{
					e.yOffset+=e.h+1;
				}
				e.targetY=e.y;
				e.aiState=aiCAR;
			break;
			
			case CAR3:
				e.x--;
				e.y=((e.y>>3)<<3);
				
				e.depth=0;
				if (e.y>=100) 
				{
					e.yOffset+=e.h+1;
				}
				e.targetY=e.y;
				e.aiState=aiCAR;
			break;			
			
			
			case GARAGE:
				e.aiState=aiIdle;
				if (Globals.getRandom(100)>50) e.aiState=aiGarageClosed;
			break;
			
			case SUBWAYPILAR:
				// place it nicely tile-wise (for collision detection stuff)
				e.x=((e.x>>3)<<3)+4;
			break;
			
		}
	
		e.activated=true;
	}
	
	
	/*
	 * ================
	 *  called after map generation is done, so some objects can turn 
	 *  them selves into "solid" wall type objects before the player starts
	 * ================
	 */
	public final static void solidify(Monster e,World myWorld) 
	{
		
		switch (e.subType)
		{
		
			case TREE:
				myWorld.putarea(e.startX+1, e.startY+5, 1,1, 32);
			break;
			
			case CAR1:
				myWorld.putarea(e.startX, e.startY+1, 1,2, 4);
				myWorld.putarea(e.startX+1, e.startY+1, 3,2, 8);
			break;

			case CAR2:
				myWorld.putarea(e.startX, e.startY+1, 1,2, 4);
				myWorld.putarea(e.startX+1, e.startY+1, 3,2, 8);
			break;

			case CAR3:
				myWorld.putarea(e.startX, e.startY+1, 1,2, 4);
				myWorld.putarea(e.startX+1, e.startY+1, 3,2, 8);
			break;
			
			case GARAGE:
				myWorld.putarea(e.startX, e.startY+3, 7, 1, 128);
			break;			
			
			case SUBWAYPILAR:
				myWorld.putarea(e.startX+1, e.startY+(e.h>>3)-1, 1,1, 128);
			break;
			
		}
		
	}
	
	
	
	/*
	 * ================
	 * update specified monster every tick
	 * ================
	 * 	
	 */
	public final static void update(Monster e, World myWorld, Player tmpPlayer) 
	{
		switch (e.aiState)
		{
			case aiIdle:
			break;
			
			case aiCAR:
				if (tmpPlayer.onGround && tmpPlayer.z>=4 
						&& tmpPlayer.x+7<e.x+e.w && tmpPlayer.x+2>=e.x 
						&& tmpPlayer.y+tmpPlayer.h>=e.y && tmpPlayer.y<e.y+12)
				{
					e.h=properties[e.subType][propH]-1;
					e.y=e.targetY+1;
					e.floatY=e.y<<4;
				}
				else
				{
					e.h=properties[e.subType][propH];
					e.y=e.targetY;
					e.floatY=e.y<<4;
				}
			break;
			
			
			case aiGarageClosed:
				if (World.enemyCount>=8 || myWorld.CameraTakeOver) return;
				
				if (tmpPlayer.x>=e.x-8 && tmpPlayer.x<e.x+8)
				{
					e.visible=true;
					e.aiState=aiGarageOpen;
					e.aiCountdown=5;
				}
			break;
			
			
			case aiGarageOpen:
				e.yOffset++;
				e.h--;
				
				if (e.h<=20)
				{
					myWorld.putarea(e.startX+1, e.startY+3, 6, 1, 0);	

					if (e.aiCountdown>0)
					{
						e.aiCountdown--;
						Monster.addMonster(Monster.mBRAWLER, e.x+10+Globals.getRandom(36), e.y+16+Globals.getRandom(4), Brawlers.BALD+Globals.getRandom(2), myWorld);
					}
				}
				if (e.h<=0)
				{
					e.died=true;
				}
			break;
			
			
			case aiTreetop:
				if (e.fireDelay>0) e.fireDelay--;
				
				if (e.hitPlayer(tmpPlayer) && tmpPlayer.z>8)
				{
					if (e.fireDelay==0) 
					{
						e.fireDelay=8;
						FX.addFX(FX.fPUFF, e.x+e.myRandom(e.w), tmpPlayer.y+e.myRandom(8), FX.puffLeave, myWorld);
					}
				}
			break;
		}
	}
	
	
}
