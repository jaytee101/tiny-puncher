package com.orangepixel.tinypuncher;

import com.orangepixel.utils.Render;

/**
 * guess!
 * 
 * @author orangepascal / orangepixel.net
 * twitter @orangepascal
 *
 */
public class Player extends Entity {
	

	public final static int		aiNormal = 0,
								aiHurl = 1,
								aiOnFloor = 2,
								aiDied = 3;
			
	
	public final static int		animIdle = 0,
								animWalking = 1,
								animBrawl = 2,
								animHurlThruAir = 3,
								animFlatOnFloor = 4,
								animJumpUp = 5,
								animFallDown = 6;
								
	
	public final static int[][] animationFrames = new int[][] {
			{ 0,1,2 },
			{ 4,5 },
			{ 6,7,8,9 }, // { 6,7,6,7,6,7,6,7,8,7,6,7,9,8,6,9 },
			{ 10 },
			{ 11 },
			{ 2 },
			{ 12 }
	};
	
	
	
	// location and movement
	public int			xSpeed;
	public int			ySpeed;
	public int			floatZ;
	public int			z;
	public int			zSpeed;	// jumping

	int					actionDelay;
	
	
	// visibility
	public int					avatar;
	public int					Frame;
	public int					FrameAdd;
	public int					FrameDelay;
	public int					MirrorFrame;
	public int					myDirection;
	public int					animationType;
	public int					animationIndex;


	// player interaction
	public boolean				actionPressed;
	public boolean				actionReleased;
	public boolean				jumpPressed;
	public boolean				leftPressed;
	public boolean				rightPressed;
	public boolean				upPressed;
	public boolean				downPressed;


	// states
	public boolean				onGround;
	boolean						Died;
	int							invincableCounter;
	public boolean				visible;
	public boolean				deleted;
	public int					score;
	

	
	
	/* ==============
	 * reset playersto deleted state
	 * ==============
	 */
	public final void init() {
		deleted=false;
		visible=true;
		
		avatar=0;
		
		w=10;
		h=10;
	}
	

	
	
	

	/* ==============
	 * hard set the player at specified world coordinates
	 * ==============
	 */
	public void setPlayerPosition(int aX, int aY) {
		floatX=aX;
		floatY=aY;
		floatZ=0;
		x=floatX>>4;
		y=floatY>>4;
		z=floatZ>>4;
	}
	

	/* ==============
	 * spawn player at specified world coordinates
	 * ==============
	 */
	public void spawnPlayer(int aX, int aY) {
		if (aX<0 || aY<0) 
		{
			x=-aX;
			y=-aY;
		} 
		else 
		{
			x=aX<<3;
			y=aY<<3;
		}
		
		x+=4;
		y+=4;
		
		z=0;
		floatZ=z<<4;
		
		setPlayerPosition(x<<4, y<<4);
		
		xSpeed=0;
		ySpeed=0;
		zSpeed=0;
		actionDelay=0;
		
		Frame=0;
		FrameAdd=16;
		MirrorFrame=0;
		
		animationType=animIdle;
		animationIndex=0;

		w=10;
		h=10;
		
		resetInput();
		actionReleased=false;
		
		aiState=aiNormal;
		alpha=255;
	}
	
	
	
	/* ==============
	 * initialise player for a new game session (reset score, health, etc)
	 * ==============
	 */
	public void gameInit() {
		deleted=false;
		
		xSpeed=0;
		ySpeed=0;
		zSpeed=0;
		
		visible=true;
		
		Frame=myRandom(2)*16;
		FrameDelay=0;
		FrameAdd=18;
		MirrorFrame=0;
		myDirection=Globals.RIGHT;
		
		aiState=aiNormal;
		
		// generate a unique random int for each monster
		int t=(UID*x) ^ ((y<<UID) << 11);
		myRandom=myRandom ^ (myRandom >> 19) ^ (t ^ (t >> 8));

		Died=false;
		
		invincableCounter=0;
		
		maxStamina=8;
		stamina=maxStamina;
		
		score=0;
		maxLife=16;
		life=maxLife;
	}
	
	

	
	/* ==============
	 * initialise players death
	 * ==============
	 */
	public void initDie() {
		if (Died) return;
		Died=true;
	}


	
	/* ==============
	 * update player object, call every tick
	 * ==============
	 */
	public void update(World myWorld) {

		if (invincableCounter>0) invincableCounter--;
		
		animationType=animIdle;

		
		switch (aiState)
		{
			case aiNormal:
				if (rightPressed)
				{
					xSpeed=16;
					MirrorFrame=0;
					animationType=animWalking;
					myDirection=Globals.RIGHT;
				}
				else if (leftPressed)
				{
					xSpeed=-16;
					MirrorFrame=h;
					animationType=animWalking;
					myDirection=Globals.LEFT;
				}
				else
				{
					if (xSpeed<0)
					{
						xSpeed+=8;
						if (xSpeed>=0) xSpeed=0;
					}
					else if (xSpeed>0)
					{
						xSpeed-=8;
						if (xSpeed<=0) xSpeed=0;
					}

					animationType=animIdle;
				}
				
				
				doHorizontal(myWorld);
				
				if (jumpPressed && onGround)
				{
					zSpeed=48;
					onGround=false;
					animationType=animJumpUp;
				}

				doGravity(myWorld);
				
				if (zSpeed>-64) zSpeed-=4;
				if (zSpeed<-4) animationType=animFallDown;
				
				
				if (upPressed)
				{
					ySpeed=-16;
					animationType=animWalking;
				}
				else if (downPressed)
				{
					ySpeed=16;
					animationType=animWalking;
				}
				else
				{
					ySpeed=0;
				}

				
				doVertical(myWorld);
				

				if (actionPressed && actionReleased)
				{
					
					if (actionDelay==0)
					{
						Bullets.addBullet(Bullets.bOWNER_PLAYER, 1+myRandom(2), Bullets.EXPLOSION, x+5, y+5, 14, myDirection,0, myWorld);
						
						Audio.playRandomWhoosh();
						
						actionDelay=6;
						animationIndex=myRandom(animationFrames[animBrawl].length);

						if (animationFrames[animBrawl][animationIndex]==8 && onGround)
						{
							zSpeed=24;
							onGround=false;
						} 
						else if (animationFrames[animBrawl][animationIndex]==9)
						{
							if (myDirection==Globals.RIGHT) xSpeed=48;
							else xSpeed=-48;
						}
						else
						{
							if (myDirection==Globals.RIGHT) xSpeed=16;
							else xSpeed=-16;
						}
							

						
						doHorizontal(myWorld);
						
						FrameDelay=0;
						actionReleased=false;
					}
					
					
					
					
				} else if (!actionPressed)
				{
					actionReleased=true;
				}
				
				if (actionDelay>0) 
				{
					actionDelay--;
					animationType=animBrawl;
				}
				
			break;
			
			
			
			case aiHurl:
				invincableCounter=16;
				animationType=animHurlThruAir;
				ySpeed=0;
				
				if (zSpeed>-48) 
				{
					zSpeed-=4;
				}
				floatZ+=zSpeed;
				
				if (floatZ<=0)
				{
					zSpeed=0;
					floatZ=0;
					
					World.worldShake=8;
					aiState=aiOnFloor;
					aiCountdown=64;
					
					FX.addFX(FX.fPUFF, x,y+h-4, 0, myWorld);
				}
				z=floatZ>>4;
				
				
				doHorizontal(myWorld);
				doVertical(myWorld);
			break;
			
			
			
			
			case aiOnFloor:
				invincableCounter=16;
				animationType=animFlatOnFloor;
				
				if (aiCountdown>0)
				{
					aiCountdown--;
				}
				else
				{
					if (life<=0)
					{
						aiState=aiDied;
						aiCountdown=16;
					}
					else
					{
						stamina=maxStamina;
						aiState=aiNormal;
					}
				}
				
				xSpeed=0;
			break;
			
			
			
			
			case aiDied:
				if (aiCountdown>0) aiCountdown--;
				else died=true;
				
				visible=(aiCountdown%4<2);
			break;
			
			
		}
		


		
		
		
		doAnimation();
		
//		if (z==0)
//		{
			FX.addFX(FX.fFLOORMARK, x, y+7, FX.floorShade, myWorld);
//		}

		resetInput();
	}
	
	
	private final void resetInput() {
		leftPressed=false;
		rightPressed=false;
		upPressed=false;
		downPressed=false;
		actionPressed=false;
		jumpPressed=false;
	}
	
	


	/* ==============
	 * animate player frames based on current state
	 * ==============
	 */
	private final void doAnimation() {

		if (FrameDelay>0)
		{
			FrameDelay--;
		}
		else
		{
			FrameDelay=8;
			animationIndex++;
		}
			
		if (animationIndex>animationFrames[animationType].length-1) animationIndex=0;
		Frame=animationFrames[animationType][animationIndex]*w;
		
		/*
		if (animationFrames[animationType][animationIndex]==8)
		{
			if (myDirection==Globals.RIGHT)
			{
				FX.addFX(FX.fPUFF, x+6, y-3, FX.puffBoxUpRight, null);
			}
			else
			{
				FX.addFX(FX.fPUFF, x-2, y-3, FX.puffBoxUpLeft, null);
			}
		} 
		else if (animationFrames[animationType][animationIndex]==9)
		{
			if (myDirection==Globals.RIGHT)
			{
				FX.addFX(FX.fPUFF, x+5, y+5, FX.puffKickRight, null);
			}
			else
			{
				FX.addFX(FX.fPUFF, x-2, y+5, FX.puffKickLeft, null);
			}
		}
		*/
		
	}
	
	
	/* ==============
	 * handle horizontal movement based on current speed
	 * ==============
	 */
	private final void doHorizontal(World myWorld) {
		int tx;
		int ty;
		
		floatX+=xSpeed;
		x=floatX>>4;
		ty=(y+h-2)>>3;
		
		if (x<0) 
		{
			x=0;
			floatX=0;
			xSpeed=0;
		}
		else if (x>(World.tileMapW<<3)-11) 
		{
			x=(World.tileMapW<<3)-11;
			floatX=x<<3;
			xSpeed=0;
		}
			
			
		if (xSpeed<0) 
		{
			tx=x>>3;
			if (myWorld.isSolid(tx,ty,3+z)) 
			{
				x=(tx<<3)+8;
				floatX=x<<4;
			}
		} 
		else if (xSpeed>0) 
		{
			tx=(x+w)>>3;
			if (myWorld.isSolid(tx,ty,3+z)) 
			{
				x=(tx<<3)-w;
				floatX=x<<4;
			}
		}
	}
	
	/* =============
	 * handle jumping (z-axis)
	 * =============
	 */
	private final void doGravity(World myWorld) {
		floatZ+=zSpeed;
		z=floatZ>>4;
				
		if (z<=0)
		{
			z=0;
			floatZ=0;
			zSpeed=0;
			onGround=true;
		}

		if (zSpeed<=0) 
		{
			int ty=(y+h-2)>>3;
			int tx=(x+w-2)>>3;
			int tx2=(x+2)>>3;
		
			if (myWorld.isSolid(tx,ty,z)) 
			{
				if (!onGround) Audio.playSoundPitched(Audio.FX_LAND1);

				onGround=true;
				z=myWorld.getTile(tx, ty);
				floatZ=z<<4;
				zSpeed=0;
			}
			else if (myWorld.isSolid(tx2,ty,z))
			{
				if (!onGround) Audio.playSoundPitched(Audio.FX_LAND1);

				onGround=true;
				z=myWorld.getTile(tx2, ty);
				floatZ=z<<4;
				zSpeed=0;
			}
			else
			{
				onGround=false;
			}
		}		
	}
	
	/* ==============
	 * handle  vertical movement based on current speed
	 * ==============
	 */
	private final void doVertical(World myWorld) {
		int tx;
		int tx2;
		int ty;
		
		floatY+=ySpeed;
		y=floatY>>4;
		tx=(x+w-2)>>3;
		tx2=(x+2)>>3;
		
		if (ySpeed<0) 
		{
			ty=(y+h-2)>>3;
			if (myWorld.isSolid(tx,ty,3+z) || myWorld.isSolid(tx2,ty,3+z)) 
			{
				y=(ty<<3)+8-(h-2);
				floatY=y<<4;
				ySpeed=0;
			}
		} 
		else if (ySpeed>=0) 
		{
			ty=(y+h-1)>>3;
			if (myWorld.isSolid(tx,ty,3+z) || myWorld.isSolid(tx2,ty,3+z)) 
			{
				y=((ty<<3)-(h-1));
				floatY=y<<4;
				ySpeed=0;
			}
		}

	}	
	
	
	/* ==============
	 * called by bullets and monsters who hit us
	 * ==============
	 */
	public final void hit(Bullets myBullet, World myWorld) {
		if (Died || myWorld.CameraTakeOver || invincableCounter>0) return;
		
		Audio.playRandomThump();
		
		
		stamina-=myBullet.energy;
		if (stamina<=0)
		{
			stamina=maxStamina;
			life--;
			invincableCounter=16;
			
			Audio.playRandomMaleOuch();
			
			/*
			aiState=aiHurl;
			zSpeed=-(32+myRandom(8));
			
			
			if (myBullet.xSpeed<0) 
			{
				xSpeed=-(16+myRandom(8));
				MirrorFrame=h;
			}
			else
			{
				xSpeed=(16+myRandom(8));
				MirrorFrame=0;
			}		
			*/	
		}
		else
		{
			if (Globals.getRandomForcedUnseeded(100)>80) Audio.playRandomMaleOuch();

		}
		
	}
	


	
	public final static void addPlayerToSpriter(Player tmpPlayer, int transparency) {
		if (!tmpPlayer.visible) return;

		int characterFrame=(64+(tmpPlayer.avatar*20)); //+tmpPlayer.MirrorFrame;
		
		int tx=tmpPlayer.x-World.offsetX;
		int ty=(tmpPlayer.y-tmpPlayer.z)-World.offsetY;

		Render.dest.set(tx,ty,tx+tmpPlayer.w,ty+tmpPlayer.h);
		Render.src.set(tmpPlayer.Frame, characterFrame, tmpPlayer.Frame+tmpPlayer.w, characterFrame+tmpPlayer.h);
		Spriter.addSprite(Render.dest,Render.src, 4, transparency, 0,2+(tmpPlayer.z<<2) , (tmpPlayer.myDirection==Globals.LEFT));


	}
	
	

}
