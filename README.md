![tinypuncher_banner.png](https://bitbucket.org/repo/xojndK/images/3768565374-tinypuncher_banner.png)
# what is this #

Tiny Puncher is a little prototype of a "90's beat em up". The core of the gameplay is there with you beating and kicking other guys.

It's really a prototype code, so the basics are there but not much else. There is the beginning of a level and some graphics for a possible 2nd level. One basic enemy type, and that's about it.

As a prototype it was fun to create, but I don't see the fun in creating a full game out of it (I figured out that I think beat-em-ups kinda have boring gameplay to begin with).

So donating the code to learning! I tried to keep the code fairly clean and easy to read, so it might help some beginning game-devs. There's a lot to tinker with, so enjoy!

See the prototype in action: [https://www.youtube.com/watch?v=42Z_Li4Tch8](https://www.youtube.com/watch?v=42Z_Li4Tch8)

# what can we do with it#

Learn from it, laugh at it, ignore it, but mostly if you are a beginning game developer: modify the shit out of this code.. that's the best way to learn programming. Play the game, find something you think would be cool to change, and hunt down the part of the code that does that!
Don't worry about how something "should" be done, the key to creating games is writing code that does what you want it to do. Improving, simplifying, enhancing, or optimizing all comes later.

# HOW!? #

Download a copy of Eclipse (free), import this project (Java), and figure it out :)
I don't have time to teach people stuff, or help people out.. I'm sure you can figure this out on your own, that's part of the fun!

You can always drop by on the [Orangepixel forum](http://www.orangepixel.net/forum/)